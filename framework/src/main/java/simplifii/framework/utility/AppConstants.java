package simplifii.framework.utility;

import android.content.Intent;

import java.util.HashMap;
import java.util.LinkedHashMap;

public interface AppConstants {

    public static final String DEF_REGULAR_FONT = "Lato-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    LinkedHashMap<Integer, String> storeCategory = new LinkedHashMap<Integer, String>();


    public static interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
        String POLLID = "pollId";
        String POLL_OPINION = "Poll Opinion";
    }

    public static interface TASK_CODES {

        int LOGIN = 10;
        int HOME = 11;
        int RESIDENT = 12;
        int FAMILY_MEMBER = 13;
        int CALENDER = 14;
        int INVITATION = 15;
        int NOTICEBOARD = 16;
        int PARKINGALLOTMENT = 17;
        int POLLDATA1 = 18;
        int POLLDATA2 = 19;
        int TENANT = 20;
        int VENDORHELP = 21;
        int COMMITTEESTAFF = 22;
        int SERVENTDRIVER = 23;

        int COMPLAINSTATUS = 24;

        int PHOTO = 24;
        int VIDEO = 25;
        int USER_PROFILE = 26;

        int EMERGENCY_LIST = 27;
        int EMERGENCY_ENTRY = 28;
        int COMPDROPLIST = 29;
        int SUBMIT_OPINION = 30;
        int MAINTENANCE = 31;
        int SEND_TICKET = 32;
        int CONFIRM_PAYMENT = 33;
        int BILL_PAYMENT = 34;
        int INV_MEMBERS_LIST = 35;
        int INVITATION_ENTRY = 36;
        int PHOTO_PROFILE = 37;
        int BILL_PENDING_LIST = 38;
    }

    public static interface TABLES_NAME {
        String RESIDENT = "mbCmembers";
        String FAMILY_MEMBERS = "fmCfamilymembers";
        String CALENDER = "caLcalendar";
    }

    public static interface ERROR_CODES {

        public static final int UNKNOWN_ERROR = 0;
        public static final int NO_INTERNET_ERROR = 1;
        public static final int NETWORK_SLOW_ERROR = 2;
        public static final int URL_INVALID = 3;
        public static final int DEVELOPMENT_ERROR = 4;

    }

    public static interface PAGE_URL {
        String PHOTO_URL = "https://society.osoeasy.in/";
        String _URL = "http://osoapi.azurewebsites.net/api/";

        String LOGIN_URL = _URL + "Login";
        String DASH_BOARD_URL = _URL + "Dashboard";
        String USER_PROFILE = _URL + "UserProfile";
        String BASE_URL = _URL + "operation";
        String RESIDENT_URL = _URL + "ResidentsList";

        String COMPLAIN_STATUS = _URL + "ComplaintsStatusList";

        String PARKING_ALLOTMENT = _URL + "ParkingAllotment";

        String EASY_POLL_1 = _URL + "OpinionPoll";
        String VENDOR_HELP = _URL + "HelpList";
        String COMMITTEE_STAFF = _URL + "CommitteeStaffs";
        String POLL_RESULT = _URL + "OpinionPollResult";
        String INVITATION_LIST = _URL + "InvitationList";
        String PHOTO_LIST = _URL + "PhotoGalleryList";
        String VIDEO_LIST = _URL + "VideoList";

        String EMERGENCY_LIST = _URL + "EmergencyList";
        String EMERGENCY_ENTRY = _URL + "EmergencySingleEntry";
        String OPINION_POLL_SUBMIT = _URL + "OpinionPollSubmit";
        String SERVICE_STATUS = _URL + "ServiceStatus";
        String COMP_DROPDOWN = _URL + "CompType";
        String SEND_TICKET = _URL + "ComplaintsTicketLog";
        String CONFIRM_PAYMENT = _URL + "CCAvenuerPayConfirm";
        String CONFIRM_PAYMENT_HDFC = _URL + "HDFCPayConfirm";
        String BILL_PAYMENT_HDFC = _URL + "BillPaymentHDFC";
        String INV_MEMBERS_LIST = _URL + "InvitationMembers";
        String INVITATION_ENTRY = _URL + "InvitationEntry";
        String NOTICE_BOARD = _URL + "NoticeBoard";
        String SERVANT_DRIVER = _URL + "ServentDriver";
        String TENANTS = _URL + "Tenants";
        String FAMILY_LIST = _URL + "FamilyList";
        String PHOTO_PROFILE = _URL + "PhotoProfile";
        String BillPandingList = _URL + "BillPandingList";
    }

    public static interface PREF_KEYS {

        String ACCESS_CODE = "access";
        String APP_LINK = "appLink";


        String IS_LOGIN = "is_login";

        String USER_PROFILE = "user_profile";
        String EMAIL = "email";
        String NAME = "name";
        String ROLE = "role";
    }

    public static interface BUNDLE_KEYS {
        public static final String KEY_SERIALIZABLE_OBJECT = "KEY_SERIALIZABLE_OBJECT";
        public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String NUMBER = "number";
        String SEARCH = "search";
        String MEMBER_POSITION = "memberPosition";
        java.lang.String KEY_TITLE = "title";
        java.lang.String KEY_DESC = "desc";
        java.lang.String KEY_DATE = "date";
        String KEY_LAT = "lattitude";
        String KEY_LNG = "longitude";
        String IS_CLOSED = "isClosed";
        String LIST_KEYS = "keys";
        String LIST_VALUES = "values";
        String KEY_LINKED_MAP = "linkedMap";
        String BILLING_TYPE_SOCIETY = "billingTypeScoiety";
        String DESCRIPTION = "Description";
        String IS_SUCCESS = "isSucess";
        String AMOUNT = "amount";
        String PAYMENT_ID = "paymentId";
    }

    public static interface FRAGMENT_TYPE {
        int HOME_FRAGMENT = 0;
        int CALENDAR = 1;
        int EASY_POLL = 2;
        int DIRECTORY = 3;
        int FAMILY = 4;
        int NOTICE_BOARD = 5;
        int INVITATION = 6;
        int COMM_GALLERY = 7;
        int COMPLAINTS = 9;
        int MAINTAINENCE = 10;
        int VIDEO = 11;
        int PHOTOS = 12;
        int PARKING = 13;
        int LOGOUT = 14;
        int RESIDENTS = 15;
        int MY_PROFILE = 16;
        int SOCIETY_BILL_PAYMENT = 17;
    }


    public static interface VIEW_TYPE {
        int CARD_MY_TEAM = 0;
    }

}
