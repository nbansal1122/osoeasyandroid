package simplifii.framework.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;

/**
 * Created by nitin on 04/12/15.
 */
public class GenericBroadcaster extends BroadcastReceiver {

    public static final String ACTION_LOCATION_UPDATE = "ACTION_LOCATION";

    private GenericDataReceiver listener = null;

    public GenericBroadcaster(GenericDataReceiver listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (listener != null)
            listener.onReceive(intent);
    }

    public static void sendBroadcast(Context ctx, Bundle bundle, String action) {
        Intent i = new Intent(action);
        i.putExtras(bundle);
        ctx.sendBroadcast(i);
    }

    public static interface GenericDataReceiver {
        public void onReceive(Intent intent);
    }
}
