package simplifii.framework.ListAdapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import simplifii.framework.R;

public class ExpandableListAdapter<T extends ExpListModel> extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader = new ArrayList<>(); // header titles
    // child data in format of header title, child title
    private HashMap<String, List<T>> _listDataChild = new HashMap<>();
    private ExpListInterface listener;
    LayoutInflater inflater;

    public ExpandableListAdapter(Context context, List<T> dataList, ExpListInterface listener) {
        this._context = context;
        inflater = LayoutInflater.from(context);
        HashSet<String> set = new HashSet<>();
        for (T data : dataList) {
            String s = data.getHeaderString();
            set.add(s);
            List<T> newDataList;
            if (_listDataChild.containsKey(s)) {
                newDataList = _listDataChild.get(s);
            } else {
                newDataList = new ArrayList<>();
            }
            newDataList.add(data);
            _listDataChild.put(s, newDataList);
        }
        this._listDataHeader.addAll(set);
        this.listener = listener;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        Object child = getChild(groupPosition, childPosition);
        return listener.getView(convertView, child, inflater);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.header_list, null);
        }
        TextView tv = (TextView) convertView;
        tv.setText(getGroup(groupPosition).toString());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public static interface ExpListInterface {
        public View getView(View convertView, Object child, LayoutInflater inflater);
    }
}