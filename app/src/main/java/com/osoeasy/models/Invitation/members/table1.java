package com.osoeasy.models.Invitation.members;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class table1 {
    @SerializedName("table1")
    List<InvMember> userInfoList = new ArrayList<>();

    public List<InvMember> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<InvMember> userInfoList) {
        this.userInfoList = userInfoList;
    }
}
