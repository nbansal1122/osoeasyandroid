package com.osoeasy.models.Invitation.members;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
//{"Member Code":"100000000003","Member Name":"ADITYA JADHAV",
// "Flat Details":"APARTMENT WING C 102","Mobile #":"0986532417",
// "SocietyName":"KUKREJA COMPLEX HOUSING SOCIETY."}
public class InvMember {
    @SerializedName("Member Code")
    @Expose
    private String memberCode;
    @SerializedName("Member Name")
    @Expose
    private String memberName;
    @SerializedName("Mobile #")
    @Expose
    private String mobileNumber;
    @SerializedName("Flat Details")
    @Expose
    private String flatDetails;

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFlatDetails() {
        return flatDetails;
    }

    public void setFlatDetails(String flatDetails) {
        this.flatDetails = flatDetails;
    }
}