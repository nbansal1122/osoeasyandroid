package com.osoeasy.models.Invitation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
//{"SrlNo":3,"in_fmbid":"KRISHNENDU DAS","lDate":"28/08/2016","in_title"
// :"Birthday party","in_letter":"please come at banquet hall for barthday party"}
public class UserInfo {
    @SerializedName("SrlNo")
    @Expose
    private Integer srlNo;
    @SerializedName("in_fmbid")
    @Expose
    private String in_fmbid;
    @SerializedName("lDate")
    @Expose
    private String lDate;
    @SerializedName("in_title")
    @Expose
    private String in_title;
    @SerializedName("in_letter")
    @Expose
    private String in_letter;

    /**
     *
     * @return
     * The srlNo
     */
    public Integer getSrlNo() {
        return srlNo;
    }

    /**
     *
     * @param srlNo
     * The SrlNo
     */
    public void setSrlNo(Integer srlNo) {
        this.srlNo = srlNo;
    }

    /**
     *
     * @return
     * The in_fmbid
     */
    public String getIn_fmbid() {
        return in_fmbid;
    }

    /**
     *
     * @param in_fmbid
     * The in_fmbid
     */
    public void setIn_fmbid(String in_fmbid) {
        this.in_fmbid = in_fmbid;
    }

    /**
     *
     * @return
     * The lDate
     */
    public String getLDate() {
        return lDate;
    }

    /**
     *
     * @param lDate
     * The lDate
     */
    public void setLDate(String lDate) {
        this.lDate = lDate;
    }

    /**
     *
     * @return
     * The in_title
     */
    public String getIn_title() {
        return in_title;
    }

    /**
     *
     * @param in_title
     * The in_title
     */
    public void setIn_title(String in_title) {
        this.in_title = in_title;
    }

    /**
     *
     * @return
     * The in_letter
     */
    public String getIn_letter() {
        return in_letter;
    }

    /**
     *
     * @param in_letter
     * The in_letter
     */
    public void setIn_letter(String in_letter) {
        this.in_letter = in_letter;
    }

}