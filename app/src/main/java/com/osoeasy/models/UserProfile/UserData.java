package com.osoeasy.models.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by RAHU on 03-08-2016.
 */
public class UserData implements Serializable{
    @SerializedName("MemberCode")
    @Expose
    public String memberCode;
    @SerializedName("UserName")
    @Expose
    public String userName;
    @SerializedName("MembersName")
    @Expose
    public String membersName;
    @SerializedName("SocietyId")
    @Expose
    public String societyId;
    @SerializedName("FatherName")
    @Expose
    public String fatherName;
    @SerializedName("idProof")
    @Expose
    public String idProof;
    @SerializedName("idProofNo")
    @Expose
    public String idProofNo;
    @SerializedName("EmergAddress")
    @Expose
    public String emergAddress;
    @SerializedName("PayType")
    @Expose
    public String payType;
    @SerializedName("PayMode")
    @Expose
    public String payMode;
    @SerializedName("PayAmount")
    @Expose
    public Double payAmount;
    @SerializedName("isIndian")
    @Expose
    public Boolean isIndian;
    @SerializedName("isForeign")
    @Expose
    public Boolean isForeign;
    @SerializedName("isPhycially")
    @Expose
    public Boolean isPhycially;
    @SerializedName("isFreedomF")
    @Expose
    public Boolean isFreedomF;
    @SerializedName("isExserviceMan")
    @Expose
    public Boolean isExserviceMan;
    @SerializedName("isSocialActivities")
    @Expose
    public Boolean isSocialActivities;
    @SerializedName("RenewalDate")
    @Expose
    public String renewalDate;

    private String Name;
    private String Email;
    private String Address;
    private String State;
    private String City;
    private String PhoneNo;
    private String Country;

    private String SocietyName;
    private String SocietyAddress;
    private String SocietyCity;
    private String SocietyState;
    private String SocietyPinCode;
    private String SocietyCountry;

    public String getSocietyName() {
        return SocietyName;
    }

    public void setSocietyName(String societyName) {
        SocietyName = societyName;
    }

    public String getSocietyAddress() {
        return SocietyAddress;
    }

    public void setSocietyAddress(String societyAddress) {
        SocietyAddress = societyAddress;
    }

    public String getSocietyCity() {
        return SocietyCity;
    }

    public void setSocietyCity(String societyCity) {
        SocietyCity = societyCity;
    }

    public String getSocietyState() {
        return SocietyState;
    }

    public void setSocietyState(String societyState) {
        SocietyState = societyState;
    }

    public String getSocietyPinCode() {
        return SocietyPinCode;
    }

    public void setSocietyPinCode(String societyPinCode) {
        SocietyPinCode = societyPinCode;
    }

    public String getSocietyCountry() {
        return SocietyCountry;
    }

    public void setSocietyCountry(String societyCountry) {
        SocietyCountry = societyCountry;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMembersName() {
        return membersName;
    }

    public void setMembersName(String membersName) {
        this.membersName = membersName;
    }

    public String getSocietyId() {
        return societyId;
    }

    public void setSocietyId(String societyId) {
        this.societyId = societyId;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getIdProofNo() {
        return idProofNo;
    }

    public void setIdProofNo(String idProofNo) {
        this.idProofNo = idProofNo;
    }

    public String getEmergAddress() {
        return emergAddress;
    }

    public void setEmergAddress(String emergAddress) {
        this.emergAddress = emergAddress;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public Double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Double payAmount) {
        this.payAmount = payAmount;
    }

    public Boolean getIndian() {
        return isIndian;
    }

    public void setIndian(Boolean indian) {
        isIndian = indian;
    }

    public Boolean getForeign() {
        return isForeign;
    }

    public void setForeign(Boolean foreign) {
        isForeign = foreign;
    }

    public Boolean getPhycially() {
        return isPhycially;
    }

    public void setPhycially(Boolean phycially) {
        isPhycially = phycially;
    }

    public Boolean getFreedomF() {
        return isFreedomF;
    }

    public void setFreedomF(Boolean freedomF) {
        isFreedomF = freedomF;
    }

    public Boolean getExserviceMan() {
        return isExserviceMan;
    }

    public void setExserviceMan(Boolean exserviceMan) {
        isExserviceMan = exserviceMan;
    }

    public Boolean getSocialActivities() {
        return isSocialActivities;
    }

    public void setSocialActivities(Boolean socialActivities) {
        isSocialActivities = socialActivities;
    }

    public String getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(String renewalDate) {
        this.renewalDate = renewalDate;
    }
}
