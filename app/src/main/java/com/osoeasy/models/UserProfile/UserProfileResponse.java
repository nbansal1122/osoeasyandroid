package com.osoeasy.models.UserProfile;

import android.text.TextUtils;

import com.osoeasy.models.BaseApi;

import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;

/**
 * Created by nbansal2211 on 04/08/16.
 */
public class UserProfileResponse extends BaseApi {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static UserProfileResponse getUserProfile(){
        String json = Preferences.getData(Preferences.USER_PROFILE, "");
        if(!TextUtils.isEmpty(json)){
            return parseJson(json);
        }
        return null;
    }
    public static UserProfileResponse parseJson(String json){
        UserProfileResponse user = (UserProfileResponse) JsonUtil.parseJson(json, UserProfileResponse.class);
        if(user != null && !user.isError() && user.getData() != null && user.getData().getUserData() != null && user.getData().getUserData().size()>0){
            Preferences.saveData(Preferences.USER_PROFILE, json);
        }
        return user;
    }
}
