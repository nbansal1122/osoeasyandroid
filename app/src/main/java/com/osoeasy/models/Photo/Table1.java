package com.osoeasy.models.Photo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/19/16.
 */
//{"FolderName":"ImagesLibrary","PhotoName":"0000000001.png"}
public class Table1 {

    @SerializedName("FolderName")
    @Expose
    private String FolderName;
    @SerializedName("MyImage")
    @Expose
    private String PhotoName;

    public String getPhotoUrl() {
        return "https://society.osoeasy.in/" + FolderName + "/" + PhotoName;
    }

    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }

    public String getPhotoName() {
        return PhotoName;
    }

    public void setPhotoName(String photoName) {
        PhotoName = photoName;
    }
}
