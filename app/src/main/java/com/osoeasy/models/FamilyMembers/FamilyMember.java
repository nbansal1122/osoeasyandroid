package com.osoeasy.models.FamilyMembers;

import com.osoeasy.models.BaseApi;

/**
 * Created by Sahil on 7/6/16.
 */
public class FamilyMember extends BaseApi {

    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
