package com.osoeasy.models.FamilyMembers;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class    Table1 {
    @SerializedName("FMemberCode")
    @Expose
    private String fMemberCode;
    @SerializedName("FMemberName")
    @Expose
    private String fMemberName;
    @SerializedName("FlatDetails")
    @Expose
    private String flatDetails;
    @SerializedName("RegMobileNo")
    @Expose
    private String regMobileNo;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("SocietyID")
    @Expose
    private String societyID;
    @SerializedName("MemberId")
    @Expose
    private String memberId;
    @SerializedName("isLoginAccess")
    @Expose
    private Integer isLoginAccess;

    public String getfMemberCode() {
        return fMemberCode;
    }

    public void setfMemberCode(String fMemberCode) {
        this.fMemberCode = fMemberCode;
    }

    public String getfMemberName() {
        return fMemberName;
    }

    public void setfMemberName(String fMemberName) {
        this.fMemberName = fMemberName;
    }

    public String getFlatDetails() {
        return flatDetails;
    }

    public void setFlatDetails(String flatDetails) {
        this.flatDetails = flatDetails;
    }

    public String getRegMobileNo() {
        return regMobileNo;
    }

    public void setRegMobileNo(String regMobileNo) {
        this.regMobileNo = regMobileNo;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getSocietyID() {
        return societyID;
    }

    public void setSocietyID(String societyID) {
        this.societyID = societyID;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getIsLoginAccess() {
        return isLoginAccess;
    }

    public void setIsLoginAccess(Integer isLoginAccess) {
        this.isLoginAccess = isLoginAccess;
    }
}