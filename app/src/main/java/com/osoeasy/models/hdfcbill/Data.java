package com.osoeasy.models.hdfcbill;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahil on 7/7/16.
 */
public class Data {

    @SerializedName("table1")
    @Expose
    private List<BillData> table1 = new ArrayList<BillData>();

    /**
     *
     * @return
     * The userData
     */
    public List<BillData> getTable1() {
        return table1;
    }

    /**
     *
     * @param table1
     * The userData
     */
    public void setTable1(List<BillData> table1) {
        this.table1 = table1;
    }

}

