package com.osoeasy.models.hdfcbill;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/7/16.
 */
public class BillData {
//    {"BillPeriod":"201607","BillNo":"555","BillDate":"29/06/2016",
//            "UnitId":"000001","UnitName":"APARTMENT","WingId":"000004",
//            "WingName":"WING D","FlatDetails":"702","BillAmount":600.0,
//            "DueDate":"15/07/2016","TotalAmount":600.0}

    @SerializedName("DueDate")
    @Expose
    private String DueDate;
    @SerializedName("BillPeriod")
    @Expose
    private String BillPeriod;

    @SerializedName("BillNo")
    @Expose
    private String BillNo;
    @SerializedName("UnitId")
    @Expose
    private String UnitId;
    @SerializedName("UnitName")
    @Expose
    private String UnitName;
    @SerializedName("WingId")
    @Expose
    private String WingId;
    @SerializedName("WingName")
    @Expose
    private Boolean WingName;
    @SerializedName("FlatDetails")
    @Expose
    private Boolean FlatDetails;
    @SerializedName("BillAmount")
    @Expose
    private double BillAmount;
    @SerializedName("TotalAmount")
    @Expose
    private double TotalAmount;

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getBillPeriod() {
        return BillPeriod;
    }

    public void setBillPeriod(String billPeriod) {
        BillPeriod = billPeriod;
    }

    public String getBillNo() {
        return BillNo;
    }

    public void setBillNo(String billNo) {
        BillNo = billNo;
    }

    public String getUnitId() {
        return UnitId;
    }

    public void setUnitId(String unitId) {
        UnitId = unitId;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public String getWingId() {
        return WingId;
    }

    public void setWingId(String wingId) {
        WingId = wingId;
    }

    public Boolean getWingName() {
        return WingName;
    }

    public void setWingName(Boolean wingName) {
        WingName = wingName;
    }

    public Boolean getFlatDetails() {
        return FlatDetails;
    }

    public void setFlatDetails(Boolean flatDetails) {
        FlatDetails = flatDetails;
    }

    public double getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(double billAmount) {
        BillAmount = billAmount;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        TotalAmount = totalAmount;
    }
}

