package com.osoeasy.models.hdfcbill;

import com.osoeasy.models.BaseApi;

/**
 * Created by Sahil on 7/7/16.
 */
public class SocietyBillResponse extends BaseApi {

    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
