package com.osoeasy.models.CompTypeDropDownList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAHU on 03-08-2016.
 */
public class Data  {
    @SerializedName("table1")
    @Expose
    public List<Table1> table1 = new ArrayList<Table1>();

    public List<Table1> getTable1() {
        return table1;
    }

    public void setTable1(List<Table1> table1) {
        this.table1 = table1;
    }
}
