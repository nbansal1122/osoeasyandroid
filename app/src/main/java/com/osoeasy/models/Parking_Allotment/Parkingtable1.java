package com.osoeasy.models.Parking_Allotment;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sahil on 7/12/16.
 */
public class Parkingtable1 {

    @SerializedName("table1")
    List<ParkingInfo> parkingInfoList;

    public List<ParkingInfo> getParkingInfoList() {
        return parkingInfoList;
    }
}
