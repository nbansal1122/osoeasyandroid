package com.osoeasy.models.Parking_Allotment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/12/16.
 */
public class ParkingInfo {

    @SerializedName("SrlNo")
    @Expose
    private Integer srlNo;
    @SerializedName("AllotmentNo")
    @Expose
    private String allotmentNo;
    @SerializedName("RegistrationNo")
    @Expose
    private String registrationNo;
    @SerializedName("MemberCode")
    @Expose
    private String memberCode;
    @SerializedName("MemberName")
    @Expose
    private String memberName;
    @SerializedName("FlatDetails")
    @Expose
    private Object flatDetails;
    @SerializedName("VehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("ParkingAreaName")
    @Expose
    private String parkingAreaName;
    @SerializedName("SocietyName")
    @Expose
    private String societyName;

    /**
     *
     * @return
     * The srlNo
     */
    public Integer getSrlNo() {
        return srlNo;
    }

    /**
     *
     * @param srlNo
     * The SrlNo
     */
    public void setSrlNo(Integer srlNo) {
        this.srlNo = srlNo;
    }

    /**
     *
     * @return
     * The allotmentNo
     */
    public String getAllotmentNo() {
        return allotmentNo;
    }

    /**
     *
     * @param allotmentNo
     * The AllotmentNo
     */
    public void setAllotmentNo(String allotmentNo) {
        this.allotmentNo = allotmentNo;
    }

    /**
     *
     * @return
     * The registrationNo
     */
    public String getRegistrationNo() {
        return registrationNo;
    }

    /**
     *
     * @param registrationNo
     * The RegistrationNo
     */
    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    /**
     *
     * @return
     * The memberCode
     */
    public String getMemberCode() {
        return memberCode;
    }

    /**
     *
     * @param memberCode
     * The MemberCode
     */
    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    /**
     *
     * @return
     * The memberName
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     *
     * @param memberName
     * The MemberName
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
     *
     * @return
     * The flatDetails
     */
    public Object getFlatDetails() {
        return flatDetails;
    }

    /**
     *
     * @param flatDetails
     * The FlatDetails
     */
    public void setFlatDetails(Object flatDetails) {
        this.flatDetails = flatDetails;
    }

    /**
     *
     * @return
     * The vehicleType
     */
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     *
     * @param vehicleType
     * The VehicleType
     */
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    /**
     *
     * @return
     * The parkingAreaName
     */
    public String getParkingAreaName() {
        return parkingAreaName;
    }

    /**
     *
     * @param parkingAreaName
     * The ParkingAreaName
     */
    public void setParkingAreaName(String parkingAreaName) {
        this.parkingAreaName = parkingAreaName;
    }

    /**
     *
     * @return
     * The societyName
     */
    public String getSocietyName() {
        return societyName;
    }

    /**
     *
     * @param societyName
     * The SocietyName
     */
    public void setSocietyName(String societyName) {
        this.societyName = societyName;
    }
}
