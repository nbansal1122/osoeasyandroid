package com.osoeasy.models.fee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nbansal2211 on 09/11/16.
 */
public class FeeData {

    @SerializedName("Month")
    @Expose
    private String month;
    @SerializedName("Member Code")
    @Expose
    private String member_Code;
    @SerializedName("Member Name")
    @Expose
    private String member_Name;
    @SerializedName("Flat Details")
    @Expose
    private String flat_Details;
    @SerializedName("Reg. Mobile No")
    @Expose
    private String reg__Mobile_No;
    @SerializedName("Total Area")
    @Expose
    private Double total_Area;
    @SerializedName("Rate")
    @Expose
    private Double rate;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("Month Code")
    @Expose
    private String month_Code;
    @SerializedName("Society Name")
    @Expose
    private String society_Name;

    /**
     * @return The month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month The Month
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * @return The member_Code
     */
    public String getMember_Code() {
        return member_Code;
    }

    /**
     * @param member_Code The Member Code
     */
    public void setMember_Code(String member_Code) {
        this.member_Code = member_Code;
    }

    /**
     * @return The member_Name
     */
    public String getMember_Name() {
        return member_Name;
    }

    /**
     * @param member_Name The Member Name
     */
    public void setMember_Name(String member_Name) {
        this.member_Name = member_Name;
    }

    /**
     * @return The flat_Details
     */
    public String getFlat_Details() {
        return flat_Details;
    }

    /**
     * @param flat_Details The Flat Details
     */
    public void setFlat_Details(String flat_Details) {
        this.flat_Details = flat_Details;
    }

    /**
     * @return The reg__Mobile_No
     */
    public String getReg__Mobile_No() {
        return reg__Mobile_No;
    }

    /**
     * @param reg__Mobile_No The Reg. Mobile No
     */
    public void setReg__Mobile_No(String reg__Mobile_No) {
        this.reg__Mobile_No = reg__Mobile_No;
    }

    /**
     * @return The total_Area
     */
    public Double getTotal_Area() {
        return total_Area;
    }

    /**
     * @param total_Area The Total Area
     */
    public void setTotal_Area(Double total_Area) {
        this.total_Area = total_Area;
    }

    /**
     * @return The rate
     */
    public Double getRate() {
        return rate;
    }

    /**
     * @param rate The Rate
     */
    public void setRate(Double rate) {
        this.rate = rate;
    }

    /**
     * @return The amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount The Amount
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return The month_Code
     */
    public String getMonth_Code() {
        return month_Code;
    }

    /**
     * @param month_Code The Month Code
     */
    public void setMonth_Code(String month_Code) {
        this.month_Code = month_Code;
    }

    /**
     * @return The society_Name
     */
    public String getSociety_Name() {
        return society_Name;
    }

    /**
     * @param society_Name The Society Name
     */
    public void setSociety_Name(String society_Name) {
        this.society_Name = society_Name;
    }

}
