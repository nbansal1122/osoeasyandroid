package com.osoeasy.models.fee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("table1")
    @Expose
    private List<FeeData> table1 = new ArrayList<FeeData>();

    /**
     * @return The table1
     */
    public List<FeeData> getTable1() {
        return table1;
    }

    /**
     * @param table1 The table1
     */
    public void setTable1(List<FeeData> table1) {
        this.table1 = table1;
    }

}

