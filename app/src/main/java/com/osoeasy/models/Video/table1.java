package com.osoeasy.models.Video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sahil on 7/19/16.
 */
public class table1 {

    @SerializedName("vd_vdlink")
    @Expose
    private String vd_vdlink;
    @SerializedName("vd_vdtitle")
    @Expose
    private String vd_vdtitle;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     * The vd_vdlink
     */
    public String getVd_vdlink() {
        return vd_vdlink;
    }

    /**
     *
     * @param vd_vdlink
     * The vd_vdlink
     */
    public void setVd_vdlink(String vd_vdlink) {
        this.vd_vdlink = vd_vdlink;
    }

    /**
     *
     * @return
     * The vd_vdtitle
     */
    public String getVd_vdtitle() {
        return vd_vdtitle;
    }

    /**
     *
     * @param vd_vdtitle
     * The vd_vdtitle
     */
    public void setVd_vdtitle(String vd_vdtitle) {
        this.vd_vdtitle = vd_vdtitle;
    }

}

