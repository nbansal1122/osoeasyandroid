package com.osoeasy.models.Video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sahil on 7/19/16.
 */
public class Data {

    @SerializedName("table1")
    @Expose
    private List<table1> table1 = new ArrayList<table1>();

    /**
     *
     * @return
     * The userData
     */
    public List<table1> getTable1() {
        return table1;
    }

    /**
     *
     * @param table1
     * The userData
     */
    public void settable1(List<table1> table1) {
        this.table1 = table1;
    }


}
