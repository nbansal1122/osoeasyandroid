package com.osoeasy.models.CommitteeStaff;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAHU on 11-07-2016.
 */
public class CommitteeData {
    @SerializedName("table1")
    @Expose
    private List<Table1> table1 = new ArrayList<Table1>();

    /**
     *
     * @return
     * The table1
     */
    public List<Table1> getTable1() {
        return table1;
    }

    /**
     *
     * @param table1
     * The table1
     */
    public void setTable1(List<Table1> table1) {
        this.table1 = table1;
    }
}
