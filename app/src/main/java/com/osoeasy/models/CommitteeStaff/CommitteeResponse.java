package com.osoeasy.models.CommitteeStaff;

import com.osoeasy.models.BaseApi;

/**
 * Created by RAHU on 11-07-2016.
 */
public class CommitteeResponse extends BaseApi {
    CommitteeData data;

    public CommitteeData getData() {
        return data;
    }

    public void setData(CommitteeData data) {
        this.data = data;
    }
}
