package com.osoeasy.models.ServentDriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class ServentInfo {


    @SerializedName("StaffCode")
    @Expose
    public String staffCode;
    @SerializedName("StaffName")
    @Expose
    public String staffName;
    @SerializedName("Profile")
    @Expose
    public String profile;
    @SerializedName("PhoneNo")
    @Expose
    public String phoneNo;
    @SerializedName("PoliceVerNo")
    @Expose
    public String policeVerNo;
    @SerializedName("Employer")
    @Expose
    public String employer;
    @SerializedName("EmployerPhone")
    @Expose
    public String employerPhone;
    @SerializedName("SocietyID")
    @Expose
    public String societyID;

    public String getStaffCode() {
        return staffCode;
    }

    public String getStaffName() {
        return staffName;
    }

    public String getProfile() {
        return profile;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getPoliceVerNo() {
        return policeVerNo;
    }

    public String getEmployer() {
        return employer;
    }

    public String getEmployerPhone() {
        return employerPhone;
    }

    public String getSocietyID() {
        return societyID;
    }
}
