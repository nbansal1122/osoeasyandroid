package com.osoeasy.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 13-Jun-16.
 */
public class HomeData {
    List<FamilyDetail> familydetails = new ArrayList<>();
    @SerializedName("AnnouncementTable")
    @Expose
    List<Timeline> timeline = new ArrayList<>();
    @SerializedName("TotalAnnouncement")
    @Expose
    private Integer totAnnouncement;
    @SerializedName("TotalMembers")
    @Expose
    private Integer totMember;
    @SerializedName("TotalComplainPending")
    @Expose
    private Integer totComplainPending;
    @SerializedName("TotalPendingAmount")
    @Expose
    private Integer totPendingAmount;
    @SerializedName("TotalPendingAmountCount")
    @Expose
    private Integer totPendingCount;
    @SerializedName("TotalPendingApproval")
    @Expose
    private Integer totPendingApproval;
    @SerializedName("TotalPoll")
    @Expose
    private Integer totPoll;

    /**
     * @return The totAnnouncement
     */
    public Integer getTotAnnouncement() {
        return totAnnouncement;
    }

    /**
     * @param totAnnouncement The TotAnnouncement
     */
    public void setTotAnnouncement(Integer totAnnouncement) {
        this.totAnnouncement = totAnnouncement;
    }

    /**
     * @return The totMember
     */
    public Integer getTotMember() {
        return totMember;
    }

    /**
     * @param totMember The TotMember
     */
    public void setTotMember(Integer totMember) {
        this.totMember = totMember;
    }

    /**
     * @return The totComplainPending
     */
    public Integer getTotComplainPending() {
        return totComplainPending;
    }

    /**
     * @param totComplainPending The TotComplainPending
     */
    public void setTotComplainPending(Integer totComplainPending) {
        this.totComplainPending = totComplainPending;
    }

    /**
     * @return The totPendingAmount
     */
    public Integer getTotPendingAmount() {
        return totPendingAmount;
    }

    /**
     * @param totPendingAmount The TotPendingAmount
     */
    public void setTotPendingAmount(int totPendingAmount) {
        this.totPendingAmount = totPendingAmount;
    }

    /**
     * @return The totPendingCount
     */
    public Integer getTotPendingCount() {
        return totPendingCount;
    }

    /**
     * @param totPendingCount The TotPendingCount
     */
    public void setTotPendingCount(Integer totPendingCount) {
        this.totPendingCount = totPendingCount;
    }

    /**
     * @return The totPendingApproval
     */
    public Integer getTotPendingApproval() {
        return totPendingApproval;
    }

    /**
     * @param totPendingApproval The TotPendingApproval
     */
    public void setTotPendingApproval(Integer totPendingApproval) {
        this.totPendingApproval = totPendingApproval;
    }

    /**
     * @return The totPoll
     */
    public Integer getTotPoll() {
        return totPoll;
    }

    /**
     * @param totPoll The TotPoll
     */
    public void setTotPoll(Integer totPoll) {
        this.totPoll = totPoll;
    }

    public List<FamilyDetail> getFamilydetails() {
        return familydetails;
    }

    public List<Timeline> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<Timeline> timeline) {
        this.timeline = timeline;
    }
}
