package com.osoeasy.models.home;

import com.osoeasy.models.BaseApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import simplifii.framework.utility.JsonUtil;

/**
 * Created by Admin on 13-Jun-16.
 */
public class HomeResponce extends BaseApi {
    HomeData data;

    public HomeData getData() {
        return data;
    }

    public static HomeResponce parseJson(String json) throws JSONException {
        JSONObject root = new JSONObject(json);
        HomeResponce responce = new HomeResponce();
        responce.setError(root.optBoolean("error"));
        responce.setMessage(root.optString("message"));
        if (!responce.isError()) {
            JSONObject data = root.getJSONObject("data");
            HomeData homeData = new HomeData();
            if (data != null) {
                List<Timeline> timelineList = new ArrayList<>();
                JSONArray announcements = data.getJSONArray("AnnouncementTable");
                for (int i = 0; i < announcements.length(); i++) {
                    JSONObject announcement = announcements.getJSONObject(i);
                    Timeline timeline = (Timeline) JsonUtil.parseJson(announcement.toString(), Timeline.class);
                    timelineList.add(timeline);
                }
                homeData.setTimeline(timelineList);

                // parse other fields

                homeData.setTotAnnouncement(getCount("TotalAnnouncement", data));
                homeData.setTotMember(getCount("TotalMembers", data));
                homeData.setTotComplainPending(getCount("TotalComplainPending", data));
                homeData.setTotPendingAmount(getCount("TotalPendingAmount", data));
                homeData.setTotPendingCount(getCount("TotalPendingAmountCount", data));
                homeData.setTotPoll(getCount("TotalPoll", data));

            }
            responce.data = homeData;
        }
        return responce;
    }

    private static int getCount(String key, JSONObject data) throws JSONException {
        JSONArray array = data.optJSONArray(key);
        if (array != null && array.length() > 0) {
            JSONObject obj = array.getJSONObject(0);
            Iterator<String> keyIterator = obj.keys();
            if (keyIterator.hasNext()) {
                return obj.getInt(keyIterator.next());
            }
        }
        return 0;
    }
}
