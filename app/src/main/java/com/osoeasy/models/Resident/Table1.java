package com.osoeasy.models.Resident;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.ListAdapters.ExpListModel;

/**
 * Created by Sahil on 7/6/16.
 */


public class Table1 extends ExpListModel {

    @SerializedName("mbId")
    @Expose
    private String mbId;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("WingNo")
    @Expose
    private String wingNo;
    @SerializedName("UnitType")
    @Expose
    private String unitType;
    @SerializedName("FlatNo")
    @Expose
    private String flatNo;
    @SerializedName("Residence")
    @Expose
    private String residence;
    @SerializedName("FamilyName")
    @Expose
    private String familyName;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("SocietyID")
    @Expose
    private String societyID;
    @SerializedName("RelationShip")
    @Expose
    private String relationShip;
    @SerializedName("CellNo")
    @Expose
    private String cellNo;
    @SerializedName("EmailId")
    @Expose
    private String emailId;

    /**
     * @return The mbId
     */
    public String getMbId() {
        return mbId;
    }

    /**
     * @param mbId The mbId
     */
    public void setMbId(String mbId) {
        this.mbId = mbId;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The Id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The wingNo
     */
    public String getWingNo() {
        return wingNo;
    }

    /**
     * @param wingNo The WingNo
     */
    public void setWingNo(String wingNo) {
        this.wingNo = wingNo;
    }

    /**
     * @return The unitType
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * @param unitType The UnitType
     */
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    /**
     * @return The flatNo
     */
    public String getFlatNo() {
        return flatNo;
    }

    /**
     * @param flatNo The FlatNo
     */
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    /**
     * @return The residence
     */
    public String getResidence() {
        return residence;
    }

    /**
     * @param residence The Residence
     */
    public void setResidence(String residence) {
        this.residence = residence;
    }

    /**
     * @return The familyName
     */
    public Object getFamilyName() {
        return familyName;
    }

    /**
     * @param familyName The FamilyName
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The Type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The societyID
     */
    public String getSocietyID() {
        return societyID;
    }

    /**
     * @param societyID The SocietyID
     */
    public void setSocietyID(String societyID) {
        this.societyID = societyID;
    }

    /**
     * @return The relationShip
     */
    public Object getRelationShip() {
        return relationShip;
    }

    /**
     * @param relationShip The RelationShip
     */
    public void setRelationShip(String relationShip) {
        this.relationShip = relationShip;
    }

    /**
     * @return The cellNo
     */
    public String getCellNo() {
        return cellNo;
    }

    /**
     * @param cellNo The CellNo
     */
    public void setCellNo(String cellNo) {
        this.cellNo = cellNo;
    }

    /**
     * @return The emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId The EmailId
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    private String headerText;

    @Override
    public String getHeaderString() {
        if (headerText == null) {
            StringBuilder builder = new StringBuilder();
            builder.append(unitType).append(", Wing : ").append(wingNo);
            headerText = builder.toString();
        }
        return headerText;
    }
}
