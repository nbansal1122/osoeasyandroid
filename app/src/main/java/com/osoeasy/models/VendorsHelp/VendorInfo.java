package com.osoeasy.models.VendorsHelp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RAHU on 11-07-2016.
 */
public class VendorInfo {
    @SerializedName("Type")
    @Expose
    public String type;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("Name")
    @Expose
    public String name;
    @SerializedName("Description")
    @Expose
    public String description;
    @SerializedName("PhoneNo")
    @Expose
    public String phoneNo;
    @SerializedName("ContPerson")
    @Expose
    public String contPerson;

    public String getType() {
        return type;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getContPerson() {
        return contPerson;
    }
}
