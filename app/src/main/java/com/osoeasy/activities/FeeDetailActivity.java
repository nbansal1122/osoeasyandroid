package com.osoeasy.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.fee.FeeData;
import com.osoeasy.models.fee.FeeResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class FeeDetailActivity extends BaseActivity implements CustomListAdapterInterface {

    List<FeeData> list = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter<FeeData> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee_detail);
        initToolBar(getString(R.string.society_bill_details));
        setAdapter();
        getFeeDetails();
    }

    private void setAdapter() {
        listView = (ListView) findViewById(R.id.listView);
        adapter = new CustomListAdapter<>(this, R.layout.row_fee, list, this);
        listView.setAdapter(adapter);
    }

    private void getFeeDetails() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.BillPandingList, FeeResponse.class);
        executeTask(AppConstants.TASK_CODES.BILL_PENDING_LIST, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.BILL_PENDING_LIST:
                FeeResponse feeResponse = (FeeResponse) response;
                if (feeResponse != null && feeResponse.getData() != null && feeResponse.getData().getTable1() != null) {
                    list.addAll(feeResponse.getData().getTable1());
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        FeeData data = list.get(position);
        setText(holder.monthValue, data.getMonth());
        setText(holder.name, data.getMember_Name());
        setText(holder.flatDetails, data.getFlat_Details());
        setText(holder.mobileNumber, data.getReg__Mobile_No());
        setText(holder.amount, data.getAmount() + "");
        return convertView;
    }

    private void setText(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
        } else {
            textView.setText(R.string.not_mentioned);
        }

    }

    class ViewHolder {
        TextView monthValue, name, mobileNumber, flatDetails, amount;

        ViewHolder(View v) {
            monthValue = (TextView) v.findViewById(R.id.tv_month_value);
            name = (TextView) v.findViewById(R.id.tv_member_name);
            mobileNumber = (TextView) v.findViewById(R.id.tv_reg_mob_no_value);
            flatDetails = (TextView) v.findViewById(R.id.tv_flat_details_value);
            amount = (TextView) v.findViewById(R.id.tv_amount_value);
        }
    }
}
