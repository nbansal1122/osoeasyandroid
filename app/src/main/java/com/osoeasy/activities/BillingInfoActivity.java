package com.osoeasy.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.avenues.lib.testotpappnew.WebViewActivity;
import com.ebs.android.sdk.Config;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;
import com.osoeasy.R;
import com.osoeasy.models.UserProfile.UserData;

import java.util.Random;

import Utility.AvenuesParams;
import Utility.ServiceUtility;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class BillingInfoActivity extends BaseActivity {

    private String billingMobile, billingEmail;
    private Bundle b;
    private static final int ACC_ID = 20418;// Provided by EBS
    private static final String SECRET_KEY = "9de6cfe71a604789c8921a0f4af89300";
    private static String HOST_NAME = "osoeasy";

    private boolean isSocietyPayment;
    private UserData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_info);
        initToolBar(getString(R.string.title_billing_information));
        setOnClickListener(R.id.btn_submit);
        data = (UserData) getIntent().getExtras().getSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT);
        setText(data.getName(), R.id.til_name);
        setText(data.getEmail(), R.id.til_email);
        setText(data.getCity(), R.id.til_city);
        setText(data.getAddress(), R.id.til_address);
        setText(data.getPhoneNo(), R.id.til_mobile);

        isSocietyPayment = getIntent().getExtras().getBoolean(AppConstants.BUNDLE_KEYS.BILLING_TYPE_SOCIETY);
    }

    public void setText(String text, int id) {
        if (!TextUtils.isEmpty(text)) {
            TextInputLayout layout = (TextInputLayout) findViewById(id);
            layout.getEditText().setText(text);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_submit:
                if (validateData()) {
                    PaymentConfirmation.data = this.data;
                    if (!isSocietyPayment) {
                        startCCAvenuePayment();
                    } else {
                        callEbsKit();
                    }
                }
                break;
        }
    }

    private void startCCAvenuePayment() {
        b = getIntent().getExtras();
        b.putString(AvenuesParams.BILLING_ADDRESS, getTilText(R.id.til_address));
        b.putString(AvenuesParams.BILLING_NAME, getTilText(R.id.til_name));
        b.putString(AvenuesParams.BILLING_CITY, getTilText(R.id.til_city));
        b.putString(AvenuesParams.BILLING_ZIP, getTilText(R.id.til_zip));
        b.putString(AvenuesParams.BILLING_STATE, getTilText(R.id.til_state));
        b.putString(AvenuesParams.BILLING_COUNTRY, "India");
        b.putString(AvenuesParams.BILLING_EMAIL, getTilText(R.id.til_email));
        b.putString(AvenuesParams.BILLING_PHONE, getTilText(R.id.til_mobile));
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtras(b);
        startActivityForResult(intent, 1);
    }

    private void callEbsKit() {
        /**
         * Set Parameters Before Initializing the EBS Gateway, All mandatory
         * values must be provided
         */

        /** Payment Amount Details */
        // Total Amount

        String amount = getIntent().getStringExtra(AvenuesParams.AMOUNT);
        String desc = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.DESCRIPTION);
        PaymentRequest.getInstance().setTransactionAmount(
                amount);

        /** Mandatory */

        PaymentRequest.getInstance().setAccountId(ACC_ID);
        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

        // Reference No
        Random r = new Random();
        String orderId = ServiceUtility.randInt(0, 9999999) + "";
        PaymentRequest.getInstance().setReferenceNo(orderId);
        /** Mandatory */

        // Email Id
        PaymentRequest.getInstance().setBillingEmail(getTilText(R.id.til_email));

//        PaymentRequest.getInstance().setBillingEmail(email);
        /** Mandatory */

        PaymentRequest.getInstance().setFailureid("1");

        // PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));
        // System.out.println("FAILURE MESSAGE"+getResources().getString(R.string.payment_failure_message));

        /** Mandatory */

        // Currency
        PaymentRequest.getInstance().setCurrency("INR");
        /** Mandatory */

        /** Optional */
        // Your Reference No or Order Id for this transaction
        PaymentRequest.getInstance().setTransactionDescription(
                desc);

        /** Billing Details */
//        PaymentRequest.getInstance().setBillingName(Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        PaymentRequest.getInstance().setBillingName(getTilText(R.id.til_name));
        /** Optional */
        PaymentRequest.getInstance().setBillingAddress(getTilText(R.id.til_address));
        /** Optional */
        PaymentRequest.getInstance().setBillingCity(getTilText(R.id.til_city));
        /** Optional */
        PaymentRequest.getInstance().setBillingPostalCode(getTilText(R.id.til_zip));
        /** Optional */
        PaymentRequest.getInstance().setBillingState(getTilText(R.id.til_state));
        /** Optional */
        PaymentRequest.getInstance().setBillingCountry("IND");
        /** Optional */
        PaymentRequest.getInstance().setBillingPhone(getTilText(R.id.til_mobile));
        /** Optional */
////
//        /** Shipping Details */
//        PaymentRequest.getInstance().setShippingName("Test_Name");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingAddress("North Mada Street");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingCity("Chennai");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingPostalCode("600019");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingState("Tamilnadu");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingCountry("IND");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingEmail("test@testmail.com");
//        /** Optional */
//        PaymentRequest.getInstance().setShippingPhone("01234567890");
//        /** Optional */

        PaymentRequest.getInstance().setLogEnabled("1");


        /**
         * Payment option configuration
         */

        /** Optional */
        PaymentRequest.getInstance().setHidePaymentOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideCashCardOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideCreditCardOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideDebitCardOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideNetBankingOption(false);

        /** Optional */
        PaymentRequest.getInstance().setHideStoredCardOption(false);

        /**
         * Initialise parameters for dyanmic values sending from merchant
         */

//        custom_post_parameters = new ArrayList<HashMap<String, String>>();
//        HashMap<String, String> hashpostvalues = new HashMap<String, String>();
//        hashpostvalues.put("account_details", "saving");
//        hashpostvalues.put("merchant_type", "gold");
//        custom_post_parameters.add(hashpostvalues);

//        PaymentRequest.getInstance()
//                .setCustomPostValues(custom_post_parameters);
        /** Optional-Set dyanamic values */

        PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));

        EBSPayment.getInstance().init(this, ACC_ID, SECRET_KEY,
                Config.Mode.ENV_TEST, Config.Encryption.ALGORITHM_MD5, HOST_NAME);

    }

    private boolean validateData() {
        boolean validFlag = isValid(R.id.til_name) && isValid(R.id.til_address) && isValid(R.id.til_zip) && isValid(R.id.til_city) && isValid(R.id.til_state);

        if (validFlag) {
            billingMobile = getTilText(R.id.til_mobile);
            if (TextUtils.isEmpty(billingMobile) || billingMobile.length() != 10) {
                setError(R.id.til_mobile, R.string.error_invalid_mobile);
                return false;
            }
            billingEmail = getTilText(R.id.til_email);
            if (TextUtils.isEmpty(billingEmail) || !Util.isValidEmail(billingEmail)) {
                setError(R.id.til_email, R.string.error_invalid_email);
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    private boolean isValid(int tilId) {
        String text = getTilText(tilId);
        if (TextUtils.isEmpty(text)) {
            setError(tilId, R.string.error_empty_field);
            return false;
        }
        return true;
    }

    private void setError(int tilId, int errorTextId) {
        TextInputLayout layout = (TextInputLayout) findViewById(tilId);
        layout.setError(getString(errorTextId));
    }

    private String getTilText(int id) {
        TextInputLayout layout = (TextInputLayout) findViewById(id);
        return layout.getEditText().getText().toString().trim();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent i = new Intent();
        i.putExtras(b);
        setResult(resultCode, i);
        finish();
    }
}
