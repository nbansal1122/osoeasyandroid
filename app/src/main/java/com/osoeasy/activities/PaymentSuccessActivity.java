package com.osoeasy.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.osoeasy.R;

import org.json.JSONObject;

import java.util.LinkedHashMap;

import Utility.AvenuesParams;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Logger;

public class PaymentSuccessActivity extends BaseActivity {

    private String payment_id;
    private String PaymentId;
    private String MerchantRefNo;
    LinkedHashMap<String, String> map = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);

        initToolBar(getString(R.string.app_name));
        getJsonReport();
    }

    private void getJsonReport() {
        payment_id = getIntent().getStringExtra("payment_id");

        String response = payment_id;

        JSONObject jObject;
        try {
            jObject = new JSONObject(response.toString());

            String responseMessage = jObject.getString("ResponseMessage");


            PaymentId = jObject.getString("PaymentId");

            MerchantRefNo = jObject.getString("MerchantRefNo");
            String Amount = jObject.getString("Amount");
            map.put("Response Message :", responseMessage);
            String DateCreated = jObject.getString("DateCreated");
            String Mode = jObject.getString("Mode");
            String BillingName = jObject.getString("BillingName");
            map.put("Billing Name :", BillingName);
            String BillingPhone = jObject.getString("BillingPhone");
            map.put("Mobile Number :", BillingPhone);
            String BillingEmail = jObject.getString("BillingEmail");
            map.put("Email :", BillingEmail);
            String BillingAddress = jObject.getString("BillingAddress");
            map.put("Details Of Apartment :", BillingAddress);


            map.put("Description :", "OSOeasy Society Bill Payment");
            map.put("Amount :", Amount);
            map.put("Payment ID :", PaymentId);
            map.put("Transaction ID :", jObject.getString("TransactionId"));
            map.put("Merchant Reference No :", MerchantRefNo);

            String BillingCity = jObject.getString("BillingCity");
            String BillingState = jObject.getString("BillingState");
            String BillingPostalCode = jObject.getString("BillingPostalCode");
            String BillingCountry = jObject.getString("BillingCountry");

            String PaymentMode = jObject.getString("PaymentMode");
            String PaymentStatus = jObject.getString("PaymentStatus");
            map.put("Payment Status :", PaymentStatus);
            if (responseMessage.toLowerCase().contains("success")) {
                startPayment(true, Amount, jObject.getString("Description"), PaymentId);
            } else {
                startPayment(false, Amount, jObject.getString("Description"), PaymentId);
            }

            return;
        } catch (Exception e) {

        }
        finish();
    }

    private void startPayment(boolean isSuccess, String amount, String desc, String paymentId) {
        Intent i = new Intent(this, PaymentConfirmation.class);
        Bundle b = new Bundle();

        b.putBoolean(AppConstants.BUNDLE_KEYS.IS_SUCCESS, isSuccess);
        b.putString(AppConstants.BUNDLE_KEYS.AMOUNT, amount);
        b.putString(AppConstants.BUNDLE_KEYS.DESCRIPTION, desc);
        b.putString(AppConstants.BUNDLE_KEYS.PAYMENT_ID, paymentId);
        b.putBoolean(AppConstants.BUNDLE_KEYS.BILLING_TYPE_SOCIETY, true);
        if (isSuccess) {
            b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, getString(R.string.title_sucess_payment_society));
        } else {
            b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, getString(R.string.title_error_payment_society));
        }
        b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_LINKED_MAP, map);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

}
