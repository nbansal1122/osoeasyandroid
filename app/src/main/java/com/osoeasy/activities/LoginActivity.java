package com.osoeasy.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.osoeasy.R;
import com.osoeasy.models.login.LoginResponce;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setOnClickListener(R.id.btn_signin, R.id.btn_register, R.id.tv_cant_access_account);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                signin();
                break;
            case R.id.btn_register:
                openRegisterationPage();
                break;
            case R.id.tv_cant_access_account:
                break;
        }
    }

    private void openRegisterationPage(){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://society.osoeasy.in/Account/Register"));
        startActivity(i);
    }

    private void signin() {
        String email = getEditText(R.id.et_email);
        String pass = getEditText(R.id.et_password);
        if (TextUtils.isEmpty(email)) {
            setError(R.id.et_email, getString(R.string.error_empty_email));
            return;
        }
        if (TextUtils.isEmpty(pass)) {
            setError(R.id.et_password, getString(R.string.error_empty_password));
            return;
        }

        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.LOGIN_URL);

        object.addParameter("email", email);
        object.addParameter("pwd", pass);

        object.setClassType(LoginResponce.class);
        executeTask(AppConstants.TASK_CODES.LOGIN, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        Log.i("msg", "response: " + response);
        if (response != null) {
            LoginResponce loginResponce = (LoginResponce) response;
            if (loginResponce.isError()) {
                Toast.makeText(LoginActivity.this, loginResponce.getMessage(), Toast.LENGTH_SHORT).show();
                return;
            }
            Preferences.saveData(AppConstants.PREF_KEYS.EMAIL, getEditText(R.id.et_email));
            Preferences.saveData(AppConstants.PREF_KEYS.NAME, loginResponce.getData().getUserProfile().getName());
            Preferences.saveData(AppConstants.PREF_KEYS.ROLE, loginResponce.getData().getUserProfile().getRole());
            Preferences.saveData(AppConstants.PREF_KEYS.IS_LOGIN, true);
            startNextActivity(HomeActivity.class);
            finish();
        }
    }
}
