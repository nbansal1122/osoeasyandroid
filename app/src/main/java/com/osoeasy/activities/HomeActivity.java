package com.osoeasy.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.View;

import com.osoeasy.fragments.DrawerFragment;
import com.osoeasy.R;
import com.osoeasy.fragments.HomeFragment;
import com.osoeasy.fragments.NextFragmentListener;
import com.osoeasy.models.UserProfile.UserProfileResponse;
//import com.osoeasy.models.home.Timeline;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Util;

public class HomeActivity extends BaseActivity implements NextFragmentListener {

    private DrawerLayout drawerLayout;
    private DrawerFragment drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        initToolBar("OSOEASY");
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_home);
        drawerFragment = DrawerFragment.getInstance(drawerLayout, this);
        addDrawerFragment(drawerFragment, R.id.lay_drawer);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextActivity(EmergencyListActivity.class);
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        addHomeFragment();
        getUserProfile();

    }

    private void getUserProfile() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.USER_PROFILE, UserProfileResponse.class);
        executeTask(AppConstants.TASK_CODES.USER_PROFILE, obj);
    }

    private void addHomeFragment() {
        HomeFragment homeFragment = HomeFragment.getInstance();
        homeFragment.setListener(this);
        getSupportFragmentManager().beginTransaction().add(R.id.lay_fragment_container, homeFragment).commit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void addDrawerFragment(DrawerFragment drawerFragment, int lay_drawer) {
        getSupportFragmentManager().beginTransaction().add(lay_drawer, drawerFragment).commit();
    }

    private void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.lay_fragment_container, fragment).commit();

    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(Gravity.LEFT)){
            drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        }
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            showExitDialog();
        } else {
            super.onBackPressed();
        }
        drawerFragment.setCurrentPosition(0);
    }

    private void showExitDialog() {
        Util.createAlertDialog(this, getString(R.string.msg_exit_app), getString(R.string.app_name), true, "OK", "CANCEL", new Util.DialogListener() {
            @Override
            public void onOKPressed(DialogInterface dialog, int which) {
                finish();
                dialog.dismiss();
            }

            @Override
            public void onCancelPressed(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public void startNext(Fragment fragment) {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (fragment == null) {
            if (count >0) {
                super.onBackPressed();
                drawerFragment.setCurrentPosition(0);
            }
//            else {
//                getSupportFragmentManager().popBackStack();
//                getSupportFragmentManager().popBackStack(HomeFragment.class.getCanonicalName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                addHomeFragment();
//            }
        } else {
            if (count > 0) {
                getSupportFragmentManager().popBackStack();
            }
            addFragment(fragment, true);
        }
    }
}

