package com.osoeasy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;

import com.osoeasy.R;
import com.osoeasy.fragments.DateDialogFragment;

import java.util.Date;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.utility.AppConstants;

public class CreateInvite extends BaseActivity implements DateDialogFragment.DateSetListener {


    private static final int REQ_SEND_INVITE = 1;
    private String invitationTitle, inviteDesc, invDate;
    private int number = 1;
    private DatePicker picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_invite);
        initToolBar("Create Invite");
        setOnClickListener(R.id.btn_submit, R.id.inv_date);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.left_back;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.inv_date:
                openDateFragment();
                break;
            case R.id.btn_submit:
                if (isValid()) {
                    Bundle b = new Bundle();
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_DESC, inviteDesc);
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, invitationTitle);
                    b.putString(AppConstants.BUNDLE_KEYS.KEY_DATE, invDate);
                    Intent i = new Intent(this, SendInvitation.class);
                    i.putExtras(b);
                    startActivityForResult(i, REQ_SEND_INVITE);
                }
                break;
        }

    }

    private void openDateFragment() {
        DateDialogFragment f = new DateDialogFragment();
        f.setListener(this);
        f.setMinDate(new Date());
        f.show(getSupportFragmentManager(), "DatePicker");
    }

    private boolean isValid() {
        invitationTitle = getTilText(R.id.til_inv_name);
        if (TextUtils.isEmpty(invitationTitle)) {
            setError(R.id.til_inv_name, R.string.error_empty_field);
            return false;
        }
        inviteDesc = getTilText(R.id.til_inv_desc);
        if (TextUtils.isEmpty(inviteDesc)) {
            setError(R.id.til_inv_desc, R.string.error_empty_field);
            return false;
        }
        if (TextUtils.isEmpty(invDate)) {
            showToast("Please select a date");
        }
        return true;
    }

    private void setError(int tilId, int errorTextId) {
        TextInputLayout layout = (TextInputLayout) findViewById(tilId);
        layout.setError(getString(errorTextId));
    }

    private String getTilText(int id) {
        TextInputLayout layout = (TextInputLayout) findViewById(id);
        return layout.getEditText().getText().toString().trim();
    }

    @Override
    public void onDateSet(int year, int month, int day) {

        String monthString = month<10?("0"+month):(month+"");
        String dayString = day<10?("0"+day):(day+"");
        invDate = dayString + "/" + (monthString) + "/" + year;
        setText(invDate, R.id.inv_date);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
