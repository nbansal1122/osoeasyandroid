package com.osoeasy.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;

import com.osoeasy.R;

import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class AddEmergencyMember extends BaseActivity {

    private String name, relationShip, phone;
    private int number = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_emergency);
        initToolBar("Add Member");
        setOnClickListener(R.id.btn_submit);
        number = getIntent().getExtras().getInt(AppConstants.BUNDLE_KEYS.MEMBER_POSITION);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.left_back;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (isValid()) {
            addEmergency();
        }
    }

    private void addEmergency() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.EMERGENCY_ENTRY, null);
        obj.addParameter("Name", name);
        obj.addParameter("MobileNo", phone);
        obj.addParameter("Relationship", relationShip);
        executeTask(AppConstants.TASK_CODES.EMERGENCY_ENTRY, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        setResult(RESULT_OK);
        finish();
    }

    private boolean isValid() {
        name = getTilText(R.id.til_name);
        if (TextUtils.isEmpty(name)) {
            setError(R.id.til_name, R.string.error_no_name);
            return false;
        }
        phone = getTilText(R.id.til_phone);
        if (TextUtils.isEmpty(phone) || phone.length() != 10) {
            setError(R.id.til_phone, R.string.error_invalid_mobile);
            return false;
        }
        relationShip = getTilText(R.id.til_relationship);
        if (TextUtils.isEmpty(relationShip)) {
            setError(R.id.til_relationship, R.string.error_relationship);
            return false;
        }
        return true;
    }

    private void setError(int tilId, int errorTextId) {
        TextInputLayout layout = (TextInputLayout) findViewById(tilId);
        layout.setError(getString(errorTextId));
    }

    private String getTilText(int id) {
        TextInputLayout layout = (TextInputLayout) findViewById(id);
        return layout.getEditText().getText().toString().trim();
    }
}
