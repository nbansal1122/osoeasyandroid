package com.osoeasy.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.BaseApi;
import com.osoeasy.models.CompTypeDropDownList.CompDropList;
import com.osoeasy.models.CompTypeDropDownList.Data;
import com.osoeasy.models.CompTypeDropDownList.Table1;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class AddNewTicketActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, CustomListAdapterInterface {
    private CompDropList compDropList;
    private List<Table1> complaintTypeList;
    private Data data;
    private Spinner spinner;
    private List<String> complaintTypes = new ArrayList<>();
    private int selectedPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_ticket);
        initToolBar("Add New Ticket");
        setOnClickListener(R.id.tv_new_ticket_submit);
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_new_ticket_submit) {
            sendticket();
        }
    }

    private void sendticket() {
        String compType = compDropList.getData().getTable1().get(spinner.getSelectedItemPosition()).getCode();
        TextInputLayout til_subject = (TextInputLayout) findViewById(R.id.TIL_subject);
        TextInputLayout til_description = (TextInputLayout) findViewById(R.id.TIL_description);
        String subject = til_subject.getEditText().getText().toString();
        String description = til_description.getEditText().getText().toString();
        if (TextUtils.isEmpty(subject)) {
            til_subject.setError("Subject cannot be empty");
            return;
        }
        if (TextUtils.isEmpty(description)) {
            til_subject.setError("Detail cannot be empty");
            return;
        }
        HttpParamObject httpParamObject = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.SEND_TICKET, BaseApi.class);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        try {
        httpParamObject.addParameter("CompType", URLEncoder.encode(compType, "UTF-8"));
        httpParamObject.addParameter("Subject", URLEncoder.encode(subject, "UTF-8"));
        httpParamObject.addParameter("ComplaintsDetails", URLEncoder.encode(description, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        executeTask(AppConstants.TASK_CODES.SEND_TICKET, httpParamObject);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getDropListData();
    }

    private void getDropListData() {
        HttpParamObject httpParamObject = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.COMP_DROPDOWN, CompDropList.class);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        executeTask(AppConstants.TASK_CODES.COMPDROPLIST, httpParamObject);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        selectedPosition = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODES.COMPDROPLIST) {
            compDropList = (CompDropList) response;
            if (compDropList.getData() != null && compDropList.getData().getTable1() != null) {
                data = compDropList.getData();
                complaintTypeList = data.getTable1();
                setComplaintTypeList();
                CustomListAdapter customListAdapter = new CustomListAdapter(this, android.R.layout.simple_dropdown_item_1line, complaintTypes, this);
                spinner.setAdapter(customListAdapter);
            }
        }
        if(response!=null&&taskCode==AppConstants.TASK_CODES.SEND_TICKET){
            BaseApi baseApi= (BaseApi) response;
            if(baseApi!=null){
                showToast(baseApi.getMessage());
                finish();
            }

        }
    }

    private void setComplaintTypeList() {
        for (Table1 t : complaintTypeList) {
            complaintTypes.add(t.getDescription());
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        convertView = inflater.inflate(resourceID, null);
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(complaintTypes.get(position));
        return convertView;
    }
}

