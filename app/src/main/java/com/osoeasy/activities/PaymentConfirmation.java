package com.osoeasy.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.BaseApi;
import com.osoeasy.models.UserProfile.UserData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

/**
 * Created by nbansal2211 on 01/11/16.
 */

public class PaymentConfirmation extends BaseActivity implements CustomListAdapterInterface {

    public List<String> keys = new ArrayList<>();
    public List<String> values = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter<String> adapter;
    private HashMap<String, String> map;
    boolean isSocietyPayment;

    public static UserData data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirmation);
        initToolBar(getString(R.string.app_name));
        String title = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.KEY_TITLE);
        String amount = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.AMOUNT);
        boolean isSocietyPayment = getIntent().getBooleanExtra(AppConstants.BUNDLE_KEYS.BILLING_TYPE_SOCIETY, false);
        if (isSocietyPayment) {
            boolean isSucess = getIntent().getBooleanExtra(AppConstants.BUNDLE_KEYS.IS_SUCCESS, false);
            String desc = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.DESCRIPTION);
            String paymentId = getIntent().getStringExtra(AppConstants.BUNDLE_KEYS.PAYMENT_ID);
            sendPaymentStatusForHDFC(isSucess, amount, desc, paymentId);
        }
        map = (HashMap<String, String>) getIntent().getExtras().getSerializable(AppConstants.BUNDLE_KEYS.KEY_LINKED_MAP);
        setText(title, R.id.tv_payment);
        addKeyValue("Society Name", data.getSocietyName());
        addKeyValue("Society Address", data.getSocietyAddress() + ", " + data.getSocietyState() + ", " + data.getSocietyCountry());
        addKeyValue("Society City", data.getSocietyCity());
        addKeyValue("Society Pincode", data.getSocietyPinCode());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            keys.add(entry.getKey());
            values.add(entry.getValue());
        }
        listView = (ListView) findViewById(R.id.lv_payment_info);
        adapter = new CustomListAdapter<>(this, R.layout.row_payment_info, keys, this);
        listView.setAdapter(adapter);


    }

    private void addKeyValue(String key, String value) {
        keys.add(key);
        values.add(value);
    }

    private void sendPaymentStatusForHDFC(boolean isSuccess, String payAmount, String desc, String paymentId) {
        HttpParamObject object = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.CONFIRM_PAYMENT_HDFC, BaseApi.class);
        if (isSuccess) {
            object.addParameter("status", "success");
        } else {
            object.addParameter("status", "failure");
        }
        object.addParameter("BillPeriod", desc);
        object.addParameter("PaymentID", paymentId);
        object.addParameter("amount", payAmount);
        executeTask(AppConstants.TASK_CODES.CONFIRM_PAYMENT, object);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
        }
        TextView keyTv = (TextView) convertView.findViewById(R.id.tv_key);
        TextView valueTv = (TextView) convertView.findViewById(R.id.tv_value);
        keyTv.setText(keys.get(position) + "");
        valueTv.setText(values.get(position) + "");
        return convertView;
    }
}
