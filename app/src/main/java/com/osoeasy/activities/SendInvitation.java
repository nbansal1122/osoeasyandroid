package com.osoeasy.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.BaseApi;
import com.osoeasy.models.Invitation.members.InvMember;
import com.osoeasy.models.Invitation.members.InvMembersData;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;

public class SendInvitation extends BaseActivity implements CustomListAdapterInterface {

    private ListView listView;
    private CustomListAdapter<InvMember> adapter;
    private List<InvMember> membersList = new ArrayList<>();
    private int clickedPosition;

    private String invTitle, invDesc, invDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_invitation);
        initToolBar("Send Invite");
        setAdapter();
        getMembersList();

        Bundle bundle = getIntent().getExtras();
        invTitle = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_TITLE);
        invDesc = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_DESC);
        invDate = bundle.getString(AppConstants.BUNDLE_KEYS.KEY_DATE);
    }

    private void getMembersList() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.INV_MEMBERS_LIST, InvMembersData.class);
        executeTask(AppConstants.TASK_CODES.INV_MEMBERS_LIST, obj);
    }

    private void setAdapter() {
        listView = (ListView) findViewById(R.id.lv_members);
        adapter = new CustomListAdapter<>(this, R.layout.row_inv_member, membersList, this);
        listView.setAdapter(adapter);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        InvMember member = membersList.get(position);
        holder.memberName.setText(member.getMemberName());
        holder.memberId.setText(member.getMemberCode());
        holder.flatDetail.setText(member.getFlatDetails());
        holder.mobileNumber.setText(member.getMobileNumber());

        holder.btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation(position);
            }
        });

        return convertView;
    }

    private void sendInvitation(int position) {
        this.clickedPosition = position;
        InvMember member = membersList.get(position);
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.INVITATION_ENTRY, BaseApi.class);
        obj.addParameter("InviteDate", invDate);
        try {
            invDesc = URLEncoder.encode(invDesc, "UTF-8");
            invTitle = URLEncoder.encode(invTitle, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        obj.addParameter("InvitationTitle", invTitle);
        obj.addParameter("InvitationLetter", invDesc);
        obj.addParameter("MemberCode", member.getMemberCode());
        executeTask(AppConstants.TASK_CODES.INVITATION_ENTRY, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.INVITATION_ENTRY:
                BaseApi api = (BaseApi) response;
                if (api != null) {
                    if (!api.isError()) {
                        setResult(RESULT_OK);
                        showToast(getString(R.string.inv_sent_success));
                        membersList.remove(clickedPosition);
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
            case AppConstants.TASK_CODES.INV_MEMBERS_LIST:
                InvMembersData data = (InvMembersData) response;
                if(data != null && data.getData() != null && data.getData().getUserInfoList() != null){
                    membersList.clear();
                    membersList.addAll(data.getData().getUserInfoList());
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    class ViewHolder {
        TextView memberName, memberId, flatDetail, mobileNumber, btnInvite;

        public ViewHolder(View v) {
            memberName = (TextView) v.findViewById(R.id.tv_member_name);
            memberId = (TextView) v.findViewById(R.id.tv_mem_id);
            flatDetail = (TextView) v.findViewById(R.id.tv_flat_details);
            mobileNumber = (TextView) v.findViewById(R.id.tv_mobile_number);
            btnInvite = (TextView) v.findViewById(R.id.btn_invite);
        }
    }
}
