package com.osoeasy.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.osoeasy.R;
import com.osoeasy.application.AppController;
import com.osoeasy.models.emergency.EmergencyListResponse;
import com.osoeasy.models.emergency.EmergencyMember;
import com.osoeasy.services.GPSTracker;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class EmergencyListActivity extends BaseActivity implements CustomListAdapterInterface {

    private ListView listView;
    private List<EmergencyMember> members = new ArrayList<>();
    private CustomListAdapter<EmergencyMember> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_list);
        initToolBar("Emergency Alarm");
        listView = (ListView) findViewById(R.id.lv_emergency);
        adapter = new CustomListAdapter<>(this, R.layout.row_emergency_member, members, this);
        listView.setAdapter(adapter);
        getLocalEmergencyList();
        getEmergencyList();
        setOnClickListener(R.id.btn_sendEmergency);
        askForLocation();
    }

    private void askForLocation() {
        if (!AppController.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            new TedPermission(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            GPSTracker.startService();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> arrayList) {
                        }
                    })
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                    .check();
        } else {
            GPSTracker.startService();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_sendEmergency) {
            String myLocation = "My Current location is : Latitude:" + Preferences.getData(AppConstants.BUNDLE_KEYS.KEY_LAT, "") +
                    "Longitude:" + Preferences.getData(AppConstants.BUNDLE_KEYS.KEY_LNG, "");
            for (EmergencyMember emergencyMember : members) {
                sendSMS(emergencyMember.getMobileNo(), "I am in Emergency, Please help !\n" + myLocation);
            }
        }
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.left_back;
    }

    private void getLocalEmergencyList() {
        EmergencyListResponse em = EmergencyListResponse.getInstance();
        if (em != null && em.getData() != null && em.getData().getTable1() != null) {
            members.clear();
            members.addAll(em.getData().getTable1());
            adapter.notifyDataSetChanged();
        }
    }

    private void getEmergencyList() {
        HttpParamObject object = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.EMERGENCY_LIST, EmergencyListResponse.class);
        executeTask(AppConstants.TASK_CODES.EMERGENCY_LIST, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.EMERGENCY_LIST:
                if (response != null) {
                    EmergencyListResponse em = (EmergencyListResponse) response;
                    if (em.getData() != null && em.getData().getTable1() != null) {
                        members.clear();
                        members.addAll(em.getData().getTable1());
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        EmergencyMember member = members.get(position);
        holder.name.setText(member.getName());
        holder.mobile.setText(member.getMobileNo());
        holder.relShip.setText(member.getRelationShip());
        return convertView;
    }

    class ViewHolder {
        TextView name, mobile, relShip;

        ViewHolder(View v) {
            name = (TextView) v.findViewById(R.id.tv_name);
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            relShip = (TextView) v.findViewById(R.id.tv_relationship);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return true;
    }

    private void addMemberToEmeergency() {
        if (members.size() > 4) {
            showToast(getString(R.string.cannot_add_more));
        } else {
            Bundle b = new Bundle();
            b.putInt(AppConstants.BUNDLE_KEYS.MEMBER_POSITION, members.size() + 1);
            Intent i = new Intent(this, AddEmergencyMember.class);
            i.putExtras(b);
            startActivityForResult(i, 1);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addMemberToEmeergency();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            getEmergencyList();
        }
    }
}
