package com.osoeasy.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.osoeasy.R;
import com.osoeasy.fragments.DrawerFragment;
import com.osoeasy.models.PollResult.PollResult;
import com.osoeasy.models.PollResult.Table1;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/18/16.
 */
public class PollActivity extends BaseActivity implements CustomListAdapterInterface {

    private ProgressBar progressBar;
    private Bundle b;
    private String pollId;
    private List<Table1> table1List = new ArrayList<>();
    private ListView listView;
    private CustomListAdapter adapter;
    private CustomFontTextView pollTopic;
    private AlertDialog dialog;
    private String opinion;
    private int selectedOpinionPosition = 0;
    private boolean isClosed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        b = getIntent().getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poll);
        initToolBar("Poll Results");
        pollTopic = (CustomFontTextView) findViewById(R.id.tv_poll_topic);
        progressBar = (ProgressBar) findViewById(R.id.pb_opinion_poll);
        listView = (ListView) findViewById(R.id.lv_poll);

        adapter = new CustomListAdapter(this, R.layout.row_poll_opinion, table1List, this);
        listView.setAdapter(adapter);
        getData();
        isClosed = b.getBoolean(AppConstants.BUNDLE_KEYS.IS_CLOSED);
        if (!isClosed) {
            setOnClickListener(R.id.btn_add_opinion);
        } else {
            hideVisibility(R.id.btn_add_opinion);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_add_opinion) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            String s[] = new String[table1List.size()];
            for (int x = 0; x < table1List.size(); x++) {
                s[x] = table1List.get(x).getOpinion();
            }
            builder.setTitle("Select Your Opinion")
                    .setSingleChoiceItems(s, 0, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedOpinionPosition = which;
                        }
                    })
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            submitOpinion();
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    });
            dialog = builder.create();
            dialog.show();
        }

    }

    private void submitOpinion() {
        if (selectedOpinionPosition < table1List.size()) {
            HttpParamObject httpParamObject = new HttpParamObject();
            httpParamObject.setUrl(AppConstants.PAGE_URL.OPINION_POLL_SUBMIT);
            httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
            httpParamObject.addParameter("pollcode", table1List.get(selectedOpinionPosition).getOq_opid());
            httpParamObject.addParameter("PollResult", table1List.get(selectedOpinionPosition).getOpinion());
            httpParamObject.setClassType(PollResult.class);
            executeTask(AppConstants.TASK_CODES.SUBMIT_OPINION, httpParamObject);
        }
    }


    public void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.POLL_RESULT);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.addParameter("PollId", b.getString(AppConstants.PARAMS.POLLID));
        httpParamObject.setClassType(PollResult.class);
        executeTask(AppConstants.TASK_CODES.POLLDATA2, httpParamObject);
    }

    private void addDrawerFragment(DrawerFragment drawerFragment, int lay_drawer) {
        getSupportFragmentManager().beginTransaction().add(lay_drawer, drawerFragment).commit();
    }

    public void setData() {
        pollTopic.setText(table1List.get(0).getTopic());
        adapter = new CustomListAdapter(this, R.layout.row_poll_opinion, table1List, this);
        listView.setAdapter(adapter);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            final PollResult response1 = (PollResult) response;
            if (taskCode == AppConstants.TASK_CODES.SUBMIT_OPINION)
                showToast(response1.getMessage());
            if (response1.getData().getTable1().size() > 0) {
                table1List = response1.getData().getTable1();
                setData();
            }
        }
    }

    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, null);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();

        }
        Double d = table1List.get(position).getAvgCounter();


        Integer i = d.intValue();
        holder.progressBar.setMax(100);
        Log.d("valueee", "" + i + d);
        holder.progressBar.setProgress(i);

        if (i == 0) {
            holder.poll_progress.setText(table1List.get(position).getOpinion());
        } else {
            holder.poll_progress.setText(table1List.get(position).getOpinion() + " " + i + "%");
        }

        return v;
    }

    private class ViewHolder {
        private ProgressBar progressBar;
        CustomFontTextView poll_topic, poll_progress;

        ViewHolder(View v) {

            progressBar = (ProgressBar) v.findViewById(R.id.pb_opinion_poll);
            poll_progress = (CustomFontTextView) v.findViewById(R.id.tv_progress);

        }


    }

}
