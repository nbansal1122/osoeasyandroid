package com.osoeasy.application;

import android.app.Application;
import android.content.pm.PackageManager;

import simplifii.framework.utility.Preferences;

/**
 * Created by nitin on 10/04/16.
 */
public class AppController extends Application {
    private static AppController instance;

    public static AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Preferences.initSharedPreferences(this);
    }

    public static boolean checkPermission(String permission) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (AppController.getInstance().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

}
