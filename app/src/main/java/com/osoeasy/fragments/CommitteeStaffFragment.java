package com.osoeasy.fragments;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.CommitteeStaff.CommitteeResponse;
import com.osoeasy.models.CommitteeStaff.Table1;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.ExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by RAHU on 11-07-2016.
 */
public class CommitteeStaffFragment extends BaseFragment implements ExpandableListAdapter.ExpListInterface {

    private List<Table1> list = new ArrayList<>();
    ExpandableListAdapter<Table1> adapter;
    private ExpandableListView listView;

    @Override
    public void initViews() {
        getData();
    }

    private void setAdapter(){
        listView = (ExpandableListView) findView(R.id.listView);
        adapter = new ExpandableListAdapter<>(getActivity(), list, this);
        listView.setAdapter(adapter);
    }
    private void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.COMMITTEE_STAFF);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.setClassType(CommitteeResponse.class);
        executeTask(AppConstants.TASK_CODES.COMMITTEESTAFF, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        CommitteeResponse committeeResponse = (CommitteeResponse) response;
        if(committeeResponse != null && committeeResponse.getData() != null && committeeResponse.getData().getTable1() != null){
            list.clear();
            list.addAll(committeeResponse.getData().getTable1());
            setAdapter();
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_committee_staff;
    }

    @Override
    public View getView(View convertView, Object child, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_row_resident, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Table1 table = (Table1) child;
        holder.username.setText(table.getName());
        holder.email.setText(table.getEmailId());
        holder.mobile.setText(table.getPhoneNo());
        String s = TextUtils.isEmpty(table.getDept()) ? "Not Mentioned" : table.getDept();
        holder.flat_details.setText(s);
        return convertView;
    }

    private class ViewHolder {
        private TextView username, email, mobile, flat_details, keyFlatDetails;

        ViewHolder(View v) {
            username = (TextView) v.findViewById(R.id.tv_member_name);
            email = (TextView) v.findViewById(R.id.tv_email);
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            flat_details = (TextView) v.findViewById(R.id.tv_flat_details);
            keyFlatDetails = (TextView) v.findViewById(R.id.tv_key_flat_details);
            keyFlatDetails.setText("Department");
        }
    }
}
