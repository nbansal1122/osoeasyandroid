package com.osoeasy.fragments;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.NoticeBoard.UserData;
import com.osoeasy.models.NoticeBoard.UserInfo;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Sahil on 7/4/16.
 */
public class NoticeBoardFragment extends BaseFragment implements CustomListAdapterInterface {
    ListView listView;
    List<UserInfo> timelineList = new ArrayList<>();
    private CustomListAdapter<UserInfo> adapter;

    @Override
    public void initViews() {
        setAdapter();
        getData();
    }

    private void getData() {
        HttpParamObject httpparamobject = new HttpParamObject();
        httpparamobject.setUrl(AppConstants.PAGE_URL.NOTICE_BOARD);
        httpparamobject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpparamobject.setClassType(UserData.class);
        executeTask(AppConstants.TASK_CODES.NOTICEBOARD, httpparamobject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if ((response != null)) {
            UserData userData = (UserData) response;
            if (userData.getData() != null && userData.getData().getUserInfoList() != null) {
                this.timelineList.clear();
                this.timelineList.addAll(userData.getData().getUserInfoList());
                adapter.notifyDataSetChanged();
            }
//        userInfoList = userData.getData().getUserInfoList();
        }
    }


    private void setAdapter() {
        listView = (ListView) findView(R.id.lv_family_members);
        adapter = new CustomListAdapter(getActivity(), R.layout.row_home_list, timelineList, this);
        listView.setAdapter(adapter);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_family_members;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final UserInfo timeline = timelineList.get(position);
        holder.tvTitle.setText(timeline.getNtitle());
        holder.tvSubTitle.setText(Html.fromHtml(timeline.getnType() + " " + timeline.getnDate()));
        holder.tvDetail.setText(timeline.getnDetails());
        return convertView;
    }

    class Holder {
        TextView tvTitle, tvSubTitle, tvDetail;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_row_home_title);
            tvSubTitle = (TextView) view.findViewById(R.id.tv_row_home_subtitle);
            tvDetail = (TextView) view.findViewById(R.id.tv_row_home_detail);
            tvDetail.setVisibility(View.VISIBLE);
        }
    }
}
