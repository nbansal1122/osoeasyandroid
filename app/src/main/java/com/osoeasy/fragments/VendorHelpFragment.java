package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.helpline.HelpList;
import com.osoeasy.models.helpline.Table1;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by RAHU on 11-07-2016.
 */
public class VendorHelpFragment extends BaseFragment implements CustomListAdapterInterface {
    private CustomListAdapter<Table1> adapter;
    private List<Table1> list = new ArrayList<>();
    private ListView listView;

    @Override
    public void initViews() {
        setdata();
        getData();
    }

    private void getData() {
        HttpParamObject httpparamobject = new HttpParamObject();
        httpparamobject.setUrl(AppConstants.PAGE_URL.VENDOR_HELP);
        httpparamobject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpparamobject.setClassType(HelpList.class);
        executeTask(AppConstants.TASK_CODES.VENDORHELP, httpparamobject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            HelpList list = (HelpList) response;
            if (!list.getError()) {
                this.list.clear();
                this.list.addAll(list.getData().getTable1());
                adapter.notifyDataSetChanged();
            }
        }

    }

    public void setdata() {
        listView = (ListView) findView(R.id.lv_family_members);
        adapter = new CustomListAdapter(getActivity(), R.layout.fragment_row_familymembers, list, this);
        listView.setAdapter(adapter);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_family_members;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Table1 table = list.get(position);
        holder.username.setText(table.getType() + " - " + table.getDescription());
        holder.email.setText(table.getDescription() + "");
        holder.mobile.setText(table.getPhoneNo());
        holder.flat_details.setText(table.getContPerson());

        return v;
    }

    private class ViewHolder {
        private TextView username, email, mobile, flat_details, emailKey, flatKey;

        ViewHolder(View v) {
            username = (TextView) v.findViewById(R.id.tv_member_name);
            email = (TextView) v.findViewById(R.id.tv_email);
            emailKey = (TextView) v.findViewById(R.id.tv_left_email_tag);
            flatKey = (TextView) v.findViewById(R.id.tv_left_flat_key);
            emailKey.setText("Remarks : ");
            flatKey.setText("Contact Person : ");
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            flat_details = (TextView) v.findViewById(R.id.tv_flat_details);
        }
    }
}
