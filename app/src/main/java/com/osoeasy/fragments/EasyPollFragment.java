package com.osoeasy.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.osoeasy.R;
import com.osoeasy.activities.PollActivity;
import com.osoeasy.models.Polls1.PollData1;
import com.osoeasy.models.Polls1.PollInfo1;
import com.osoeasy.models.Polls2.PollData2;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/4/16.
 */
public class EasyPollFragment extends BaseFragment implements simplifii.framework.ListAdapters.CustomListAdapterInterface {

    PollData1 pollData1;
    PollData2 pollData2;
    CustomListAdapter adapter;
    List<PollInfo1> table1;
    ListView listView;
    String pollId;


    @Override
    public void initViews() {


        getDataPoll1();
    }

    private void getDataPoll2() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.POLL_RESULT);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.addParameter("PollId", "");
        httpParamObject.setClassType(PollData2.class);
        executeTask(AppConstants.TASK_CODES.POLLDATA2, httpParamObject);
    }

    private void getDataPoll1() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.EASY_POLL_1);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.setClassType(PollData1.class);
        executeTask(AppConstants.TASK_CODES.POLLDATA1, httpParamObject);
    }


    public void setData1() {

        listView = (ListView) findView(R.id.lv_easypoll);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                pollId = table1.get(position).getPollId();
                Bundle b = new Bundle();
                b.putString(AppConstants.PARAMS.POLLID, pollId);
                if("closed".equalsIgnoreCase(table1.get(position).getStatus())){
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_CLOSED, true);
                }else{
                    b.putBoolean(AppConstants.BUNDLE_KEYS.IS_CLOSED, false);
                }

                Intent i = new Intent(getActivity(), PollActivity.class);
                i.putExtras(b);
                startActivity(i);
            }
        });
        adapter = new CustomListAdapter(getActivity(), R.layout.row_easypoll, table1, this);
        listView.setAdapter(adapter);


    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        if (response != null) {
            if (taskCode == AppConstants.TASK_CODES.POLLDATA1) {
                pollData1 = (PollData1) response;
                table1 = pollData1.getData().getPollInfo1List();
                if(table1!=null)
                setData1();
            } else {
                pollData2 = (PollData2) response;
            }
        }
    }

    @Override
    public int getViewID() {
        Log.d("response", "in get View");
        return R.layout.fragment_easypoll;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.poll_result.setText(table1.get(position).getTopic());
        String publish = getdate(table1.get(position).getPublishdate());
        holder.published_date.setText(publish);
        String end = getdate(table1.get(position).getCloseDate());
        holder.end_date.setText(end);
        holder.status.setText(table1.get(position).getStatus());
        holder.votes.setText(table1.get(position).getVoteCount());


        return v;
    }

    public String getdate(String date) {
        String Date = Util.convertDateFormat(date, Util.SLASH_PATTERN_DATE, Util.MONTH_DAY_YEAR_DATE_PATTERN);
        return Date;
    }

    private class ViewHolder {
        private CustomFontTextView poll_result, published_date, end_date, status, votes;

        ViewHolder(View v) {
            poll_result = (CustomFontTextView) v.findViewById(R.id.tv_poll_result);
            published_date = (CustomFontTextView) v.findViewById(R.id.tv_published_date);
            end_date = (CustomFontTextView) v.findViewById(R.id.tv_end_date);
            status = (CustomFontTextView) v.findViewById(R.id.tv_status);
            votes = (CustomFontTextView) v.findViewById(R.id.tv_votes);
        }
    }
}
