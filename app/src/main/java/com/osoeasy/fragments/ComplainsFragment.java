package com.osoeasy.fragments;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.activities.AddNewTicketActivity;
import com.osoeasy.models.ComplainStatusList.ComplaintResponse;
import com.osoeasy.models.ComplainStatusList.Data;
import com.osoeasy.models.ComplainStatusList.Table1;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

public class ComplainsFragment extends BaseFragment implements CustomListAdapterInterface {
    private List<Table1> table1 = new ArrayList<>();
    private Data data;
    private ListView listView;
    private CustomListAdapter<Table1> customListAdapter;
    private ComplaintResponse complaintResponse;

    @Override
    public void initViews() {
        listView = (ListView) findView(R.id.lv_complain);
        setOnClickListener(R.id.tv_add_ticket);
        getdata();
        customListAdapter = new CustomListAdapter(getActivity(), R.layout.fragment_complains, table1, this);
        listView.setAdapter(customListAdapter);
    }

    private void getdata() {
        HttpParamObject httpParamObject = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.COMPLAIN_STATUS, ComplaintResponse.class);
        executeTask(AppConstants.TASK_CODES.COMPLAINSTATUS, httpParamObject);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), AddNewTicketActivity.class);

        startActivity(intent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        Table1 listOfComplain = table1.get(position);

        if (TextUtils.isEmpty(listOfComplain.getTypeName())) {
            holder.complainType.setText(listOfComplain.getTypeName());
        } else {
            holder.complainType.setText("N.A.");
        }
        if (!TextUtils.isEmpty(listOfComplain.getMemberName())) {
            holder.member_name.setText(listOfComplain.getMemberName());
        } else {
            holder.member_name.setText("N.A.");
        }
        if (!TextUtils.isEmpty(listOfComplain.getMemberCode())) {
            holder.memebr_id.setText(listOfComplain.getMemberCode());
        } else {
            holder.memebr_id.setText("N.A.");
        }
        if (!TextUtils.isEmpty(listOfComplain.getTicketDate())) {
            holder.date.setText(listOfComplain.getTicketDate());
        } else {
            holder.date.setText("N.A.");
        }
        if (TextUtils.isEmpty(listOfComplain.getTicketNo())) {
            holder.ticket_number.setText(listOfComplain.getTicketNo());
        } else {
            holder.ticket_number.setText("N.A.");
        }
        return convertView;
    }


    class Holder {
        TextView complainType;
        TextView member_name;
        TextView memebr_id;
        TextView date;
        TextView ticket_number;


        public Holder(View view) {
            complainType = (TextView) view.findViewById(R.id.tv_complain_type);
            member_name = (TextView) view.findViewById(R.id.tv_member_name);
            memebr_id = (TextView) view.findViewById(R.id.tv_member_id);
            date = (TextView) view.findViewById(R.id.tv_complain_date);
            ticket_number = (TextView) view.findViewById(R.id.tv_ticket_no);
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODES.COMPLAINSTATUS) {
            complaintResponse = (ComplaintResponse) response;
            data = complaintResponse.getData();
            if (data != null && data.getTable1() != null) {
                table1.clear();
                table1.addAll(data.getTable1());
                customListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.listview_complain;
    }
}
