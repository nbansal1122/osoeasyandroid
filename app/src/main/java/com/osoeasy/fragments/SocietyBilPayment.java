package com.osoeasy.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebs.android.sdk.Config;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;
import com.osoeasy.R;
import com.osoeasy.activities.BillingInfoActivity;
import com.osoeasy.models.UserProfile.UserData;
import com.osoeasy.models.UserProfile.UserProfile;
import com.osoeasy.models.hdfcbill.BillData;
import com.osoeasy.models.hdfcbill.SocietyBillResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import Utility.AvenuesParams;
import Utility.ServiceUtility;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

/**
 * Created by nbansal2211 on 16/08/16.
 */
public class SocietyBilPayment extends BaseFragment {

    private LinearLayout layout;

    AlertDialog dialog;
    private UserData userData;


    ArrayList<HashMap<String, String>> custom_post_parameters;

    private static final int ACC_ID = 20418;// Provided by EBS
    private static final String SECRET_KEY = "9de6cfe71a604789c8921a0f4af89300";// Provided by EBS

//    private static final int ACC_ID = 20430;// Provided by EBS
//    private static final String SECRET_KEY = "7373e85b6d2e0ca5689754ec5db42b43";// Provided by EBS

    private static final double PER_UNIT_PRICE = 1.00;
    double totalamount;
    private List<BillData> dataList;
    private BillData billData;

    @Override
    public void initViews() {
        layout = (LinearLayout) findView(R.id.ll_bill_info);
        getBillData();
    }

    private void getBillData() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.BILL_PAYMENT_HDFC, SocietyBillResponse.class);
        executeTask(AppConstants.TASK_CODES.BILL_PAYMENT, obj);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case AppConstants.TASK_CODES.BILL_PAYMENT:
                if (response != null) {
                    SocietyBillResponse res = (SocietyBillResponse) response;
                    if (res.getData() != null && res.getData().getTable1() != null && res.getData().getTable1().size() > 0) {
                        dataList = res.getData().getTable1();
                        setUpViews(dataList.get(0));
                    }
                }
                break;
            case AppConstants.TASK_CODES.USER_PROFILE:
                UserProfile userProfile = (UserProfile) response;
                if (userProfile.getData().getUserData().size() > 0) {
                    final UserData userData = userProfile.getData().getUserData().get(0);
                    if (userData != null) {
                        this.userData = userData;
                    }
                }
                break;
        }
    }

    private void getData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.USER_PROFILE);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(UserProfile.class);
        executeTask(AppConstants.TASK_CODES.USER_PROFILE, object);
    }

    private void setUpViews(BillData billData) {
        this.billData = billData;
        layout.removeAllViewsInLayout();
        addRow("Bill Number", billData.getBillNo());
        addRow("Bill Period", billData.getBillPeriod());
        addRow("Due Date", billData.getDueDate());
        addRow("Unit ID", billData.getUnitId());
        addRow("Unit Name", billData.getUnitName());
        addRow("Wing Id", billData.getWingId());
        addRow("Bill Amount", billData.getBillAmount() + "");
        addRow("Total Amount", billData.getTotalAmount() + "");
        totalamount = billData.getBillAmount();
        if (totalamount > 0) {
            getData();
            showVisibility(R.id.tv_payment);
            setOnClickListener(R.id.tv_payment);
        } else {
            hideVisibility(R.id.tv_payment);
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_payment:
                callEbsKit();
                break;
        }
    }

    private void addRow(String key, String value) {
        View row = LayoutInflater.from(getActivity()).inflate(R.layout.view_left_right, null);
        setData(key, row, R.id.tv_left);
        setData(value, row, R.id.tv_right);
        layout.addView(row);
    }

    private void setData(String text, View view, int textViewId) {
        TextView tv = (TextView) view.findViewById(textViewId);
        tv.setText(text);
    }

    @Override
    public int getViewID() {
        return R.layout.layout_society_payment;
    }

    private void callEbsKit() {

        if (userData != null) {
            Bundle b = new Bundle();
            b.putString(AvenuesParams.AMOUNT, String.format("%.2f", billData.getBillAmount()));
            b.putBoolean(AppConstants.BUNDLE_KEYS.BILLING_TYPE_SOCIETY, true);
            b.putString(AppConstants.BUNDLE_KEYS.DESCRIPTION, billData.getBillPeriod() + "");
            b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, userData);
            Intent i = new Intent(getActivity(), BillingInfoActivity.class);
            i.putExtras(b);
            startActivityForResult(i, 1);
        } else {
            showToast(getString(R.string.unable_to_find_details_of_user));
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getBillData();
    }
}
