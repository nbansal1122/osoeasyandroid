package com.osoeasy.fragments;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

public class DateDialogFragment extends DialogFragment implements OnDateSetListener {
    DateSetListener listener;
    private Date minDate;
    int pDay;
    int pMonth;
    int pYear;

    public interface DateSetListener {
        void onDateSet(int i, int i2, int i3);
    }

    public Date getMinDate() {
        return this.minDate;
    }

    public void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    public DateSetListener getListener() {
        return this.listener;
    }

    public void setListener(DateSetListener listener) {
        this.listener = listener;
    }

    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        this.pYear = year;
        this.pDay = day;
        this.pMonth = month;
        if (this.listener != null) {
            this.listener.onDateSet(year, month+1, day);
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        return new DatePickerDialog(getActivity(), this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
    }
}
