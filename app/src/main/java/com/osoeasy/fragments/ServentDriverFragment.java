package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.ServentDriver.ServentData;
import com.osoeasy.models.ServentDriver.ServentInfo;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by RAHU on 11-07-2016.
 */
public class ServentDriverFragment extends BaseFragment implements CustomListAdapterInterface {
    List<ServentInfo> list = new ArrayList<>();
    private CustomListAdapter<ServentInfo> adapter;
    private ListView listView;

    @Override
    public void initViews() {
        setAdapter();
        getData();
    }

    private void getData() {
        HttpParamObject httpparamobject = new HttpParamObject();
        httpparamobject.setUrl(AppConstants.PAGE_URL.SERVANT_DRIVER);
        httpparamobject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpparamobject.setClassType(ServentData.class);
        executeTask(AppConstants.TASK_CODES.SERVENTDRIVER, httpparamobject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if(response != null) {
            ServentData serventdata = (ServentData) response;
            if(!serventdata.isError()){
                list.clear();
                list.addAll(serventdata.getData().getServentInfoList());
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_family_members;
    }

    private void setAdapter() {
        listView = (ListView) findView(R.id.lv_family_members);
        adapter = new CustomListAdapter(getActivity(), R.layout.fragment_row_familymembers, list, this);
        listView.setAdapter(adapter);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        ServentInfo table = list.get(position);
        holder.username.setText(table.getStaffName() + " | " + table.getProfile() + " | " + table.getStaffCode());
        holder.email.setText(table.getPoliceVerNo());
        holder.mobile.setText(table.getPhoneNo());
        holder.flat_details.setText(table.getEmployer() + " | " + table.getEmployerPhone());

        return v;
    }

    private class ViewHolder {
        private TextView username, email, mobile, flat_details, emailKey, flatKey;

        ViewHolder(View v) {
            username = (TextView) v.findViewById(R.id.tv_member_name);
            email = (TextView) v.findViewById(R.id.tv_email);
            emailKey = (TextView) v.findViewById(R.id.tv_left_email_tag);
            flatKey = (TextView) v.findViewById(R.id.tv_left_flat_key);
            emailKey.setText("Verification NO : ");
            flatKey.setText("Employer : ");
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            flat_details = (TextView) v.findViewById(R.id.tv_flat_details);
        }
    }
}
