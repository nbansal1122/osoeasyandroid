package com.osoeasy.fragments;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.activities.LoginActivity;
import com.osoeasy.models.UserProfile.UserProfileResponse;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

public class DrawerFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {
    DrawerLayout drawerLayout;
    List<String> listItems = new ArrayList<>();
    List<Integer> images = new ArrayList<>();
    ListView listView;
    NextFragmentListener listener;
    private int currentPosition = 0;
    private List<DrawerItem> items = new ArrayList<>();

    public static DrawerFragment getInstance(DrawerLayout drawerLayout, NextFragmentListener listerner) {
        DrawerFragment drawerFragment = new DrawerFragment();
        drawerFragment.drawerLayout = drawerLayout;
        drawerFragment.listener = listerner;
        return drawerFragment;
    }

    @Override
    public void initViews() {
        initData();
        listView = (ListView) findView(R.id.list_drawer);
        CustomListAdapter listAdapter = new CustomListAdapter(getActivity(), R.layout.row_drawer, items, this);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
//        setProfile();
    }



    private void setProfile(){
        UserProfileResponse response = UserProfileResponse.getUserProfile();
        if(response != null){
            setText(R.id.tv_userName, response.getData().getUserData().get(0).getUserName());
        }
    }


    private void initData() {
        items.clear();
        items.add(new DrawerItem("Dashboard", R.drawable.di_dashboard, AppConstants.FRAGMENT_TYPE.HOME_FRAGMENT));
        items.add(new DrawerItem("My Profile", R.drawable.di_residents, AppConstants.FRAGMENT_TYPE.MY_PROFILE));
        items.add(new DrawerItem("Society Bill", R.drawable.di_residents, AppConstants.FRAGMENT_TYPE.SOCIETY_BILL_PAYMENT));
        items.add(new DrawerItem("Calender", R.drawable.di_calender, AppConstants.FRAGMENT_TYPE.CALENDAR));
        items.add(new DrawerItem("EasyPoll", R.drawable.di_easypoll, AppConstants.FRAGMENT_TYPE.EASY_POLL));
        items.add(new DrawerItem("Parking Allotment", R.drawable.di_parking, AppConstants.FRAGMENT_TYPE.PARKING));
        items.add(new DrawerItem("Directory", R.drawable.di_directry, AppConstants.FRAGMENT_TYPE.DIRECTORY));
        items.add(new DrawerItem("Residents", R.drawable.di_residents, AppConstants.FRAGMENT_TYPE.RESIDENTS));
        items.add(new DrawerItem("Notice Board", R.drawable.di_noticeboard, AppConstants.FRAGMENT_TYPE.NOTICE_BOARD));
        items.add(new DrawerItem("Invitation", R.drawable.di_invitation, AppConstants.FRAGMENT_TYPE.INVITATION));
        items.add(new DrawerItem("Videos", R.drawable.di_communication, AppConstants.FRAGMENT_TYPE.VIDEO));
        items.add(new DrawerItem("Photos", R.drawable.di_communication, AppConstants.FRAGMENT_TYPE.PHOTOS));
        items.add(new DrawerItem("Complaints", R.drawable.di_complains, AppConstants.FRAGMENT_TYPE.COMPLAINTS));
        items.add(new DrawerItem("Maintenance", R.drawable.di_mentainence, AppConstants.FRAGMENT_TYPE.MAINTAINENCE));
        items.add(new DrawerItem("Logout", R.drawable.logout, AppConstants.FRAGMENT_TYPE.LOGOUT));
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_drawer;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.textView.setText(items.get(position).name);
        holder.imageView.setImageResource(items.get(position).icon);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (position == currentPosition) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            return;
        } else {
            currentPosition = position;
        }

        switch (items.get(position).fragmentType) {
            case AppConstants.FRAGMENT_TYPE.HOME_FRAGMENT:
                listener.startNext(null);
                break;
            case AppConstants.FRAGMENT_TYPE.CALENDAR:
                CalenderFragment calenderFragment = CalenderFragment.getInstance(listener);
                listener.startNext(calenderFragment);
                break;

            case AppConstants.FRAGMENT_TYPE.LOGOUT:

                Preferences.deleteAllData();
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
                break;
            default:
                listener.startNext(FragmentFactory.getFragment(items.get(position).fragmentType));

        }
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    class Holder {
        TextView textView;
        ImageView imageView;

        Holder(View view) {
            textView = (TextView) view.findViewById(R.id.tv_drawer);
            imageView = (ImageView) view.findViewById(R.id.iv_drawer);
        }
    }

    private class DrawerItem{
        private String name;
        private int icon;
        private int fragmentType;

        public DrawerItem(String name, int icon, int fType){
            this.name = name;
            this.icon = icon;
            this.fragmentType = fType;
        }
    }
}
