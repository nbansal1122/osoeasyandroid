package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.Tenants.TenantData;
import com.osoeasy.models.Tenants.TenantInfo;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;


/**
 * Created by RAHU on 11-07-2016.
 */
public class Tenants extends BaseFragment implements CustomListAdapterInterface {

    List<TenantInfo> list = new ArrayList<>();
    private CustomListAdapter<TenantInfo> adapter;
    private ListView listView;

    @Override
    public void initViews() {
        setAdapter();
        getData();
    }

    private void getData() {

        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.TENANTS);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.setClassType(TenantData.class);
        executeTask(AppConstants.TASK_CODES.TENANT, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            TenantData tenantdata = (TenantData) response;
            if (!tenantdata.isError()) {
                list.clear();
                list.addAll(tenantdata.getData().getTenantInfoList());
                adapter.notifyDataSetChanged();
            }
        }

    }

    private void setAdapter() {
        listView = (ListView) findView(R.id.lv_family_members);
        adapter = new CustomListAdapter(getActivity(), R.layout.fragment_row_familymembers, list, this);
        listView.setAdapter(adapter);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_family_members;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        TenantInfo table = list.get(position);
        holder.username.setText(table.getTenantName());
        holder.email.setText(table.getEmailID());
        holder.mobile.setText(table.getMemberCode());
        holder.flat_details.setText(table.getFlatDetails());

        return v;
    }

    private class ViewHolder {
        private TextView username, email, mobile, flat_details;

        ViewHolder(View v) {
            username = (TextView) v.findViewById(R.id.tv_member_name);
            email = (TextView) v.findViewById(R.id.tv_email);
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            flat_details = (TextView) v.findViewById(R.id.tv_flat_details);
        }
    }
}
