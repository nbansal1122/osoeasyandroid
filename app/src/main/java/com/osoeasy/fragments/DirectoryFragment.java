package com.osoeasy.fragments;


import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.osoeasy.R;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomPagerAdapter;
import simplifii.framework.fragments.BaseFragment;

/**
 * Created by Sahil on 7/12/16.
 */
public class DirectoryFragment extends BaseFragment implements CustomPagerAdapter.PagerAdapterInterface, ViewPager.OnPageChangeListener{

    List<Fragment> fragmentList;
    List<String> list;
    CustomPagerAdapter adapter;

    @Override
    public void initViews() {


        list = new ArrayList<>();
        fragmentList  = new ArrayList<>();
        ViewPager viewPager = (ViewPager)findView(R.id.vp_directory);
        list.add("Resident's List");
        list.add("Committee Staff");
        list.add("Vendors Help");

        adapter = new CustomPagerAdapter(getChildFragmentManager(),list,this);
        viewPager.setAdapter(adapter);
        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip)findView(R.id.tabs);
        tabStrip.setViewPager(viewPager);

        ResidentsFragment residentsFragment = new ResidentsFragment();
        fragmentList.add(residentsFragment);
        CommitteeStaffFragment committeeStaffFragment = new CommitteeStaffFragment();
        fragmentList.add(committeeStaffFragment);
        VendorHelpFragment vendorHelpFragment = new VendorHelpFragment();
        fragmentList.add(vendorHelpFragment);



    }


    @Override
    public Fragment getFragmentItem(int position, Object listItem) {
        return fragmentList.get(position);
    }
    @Override
    public int getViewID() {
        return R.layout.fragment_directory;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public CharSequence getPageTitle(int position, Object listItem) {
        return list.get(position);
    }
}
