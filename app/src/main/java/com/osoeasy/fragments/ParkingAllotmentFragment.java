package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.osoeasy.R;
import com.osoeasy.models.Parking_Allotment.ParkingInfo;
import com.osoeasy.models.Parking_Allotment.ParkingUserData;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/4/16.
 */
public class ParkingAllotmentFragment extends BaseFragment implements CustomListAdapterInterface {
    private List<ParkingInfo> parkingInfoList;

    CustomListAdapter adapter;

    @Override
    public void initViews() {
        getData();

    }

    private void getData() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(AppConstants.PAGE_URL.PARKING_ALLOTMENT);
        httpParamObject.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        httpParamObject.setClassType(ParkingUserData.class);
        executeTask(AppConstants.TASK_CODES.PARKINGALLOTMENT, httpParamObject);
    }

    public void setdata() {
        ListView listView = (ListView) findView(R.id.lv_parking);
        adapter = new CustomListAdapter(getActivity(), R.layout.row_parking_allotment, parkingInfoList, this);
        listView.setAdapter(adapter);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            ParkingUserData parkinguserdata = (ParkingUserData) response;
            parkingInfoList = parkinguserdata.getParkingtable1().getParkingInfoList();
            if (parkingInfoList != null)
                setdata();
        }


    }

    @Override
    public int getViewID() {
        return R.layout.fragment_parking_allotment;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent, int resourceID, LayoutInflater inflater) {

        ViewHolder holder = null;
        if (v == null) {
            v = inflater.inflate(resourceID, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.mem_name.setText(parkingInfoList.get(position).getMemberName());
        holder.mem_id.setText(parkingInfoList.get(position).getMemberCode());
        holder.vehicle_type.setText(parkingInfoList.get(position).getVehicleType());
        holder.parking_area.setText(parkingInfoList.get(position).getParkingAreaName());
        holder.allotment_no.setText(parkingInfoList.get(position).getAllotmentNo());
        holder.registration_no.setText(parkingInfoList.get(position).getRegistrationNo());

        return v;
    }

    private class ViewHolder {
        private CustomFontTextView mem_name, mem_id, vehicle_type, parking_area, allotment_no, registration_no;

        ViewHolder(View v) {

            mem_name = (CustomFontTextView) v.findViewById(R.id.tv_mem_name);
            mem_id = (CustomFontTextView) v.findViewById(R.id.tv_mem_id);
            vehicle_type = (CustomFontTextView) v.findViewById(R.id.tv_vehicle);
            parking_area = (CustomFontTextView) v.findViewById(R.id.tv_parking_area);
            allotment_no = (CustomFontTextView) v.findViewById(R.id.tv_allotment_no);
            registration_no = (CustomFontTextView) v.findViewById(R.id.tv_registration_no);
        }
    }

}
