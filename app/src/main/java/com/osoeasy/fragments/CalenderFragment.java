package com.osoeasy.fragments;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.osoeasy.R;
import com.osoeasy.models.Calender.CalenderData;
import com.osoeasy.models.Calender.Table1;
import com.p_v.flexiblecalendar.FlexibleCalendarView;
import com.p_v.flexiblecalendar.view.BaseCellView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/4/16.
 */
public class CalenderFragment extends BaseFragment {

    CalenderData response;
    List<Table1> table = new ArrayList<>();
    CustomListAdapter adapter;
    FlexibleCalendarView calendarView;
    CustomFontTextView bar;
    HashMap<String, List<Table1>> map = new HashMap<>();
    NextFragmentListener listener;

    @Override
    public void initViews() {

        map.clear();
        calendarView = (FlexibleCalendarView) findView(R.id.calender_view);
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.BASE_URL);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.addParameter("tblname", AppConstants.TABLES_NAME.CALENDER);
        object.addParameter("cond", "");
        object.setClassType(CalenderData.class);
        executeTask(AppConstants.TASK_CODES.CALENDER, object);
    }

    public static CalenderFragment getInstance(NextFragmentListener listener){
        CalenderFragment calenderFragment = new CalenderFragment();
        calenderFragment.listener = listener;
        return calenderFragment;
    }
    @Override
    public int getViewID() {
        return R.layout.fragment_calender;
    }


    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            this.response = (CalenderData) response;
            this.table = ((CalenderData) response).getData().getTable1();

            createEventMap();
            setData();
        }


    }

    private void createEventMap() {

        for (Table1 data : this.table) {
            List<Table1> events = new ArrayList<>();
            if (map.containsKey(data.getCa_sdate())) {
                events = map.get(data.getCa_sdate());
            } else {
                events = new ArrayList<>();
            }
            events.add(data);
            map.put(data.getCa_sdate(), events);
        }

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        String monthDatePattern = getCurrentMonthPattern(year, month);
        setCalendarMonth(monthDatePattern);
    }

    private String getCurrentMonthPattern(int year, int month){
        return  Util.convertDateFormat(getDate(year, month, 1), Util.DISCVER_SERVER_DATE_PATTERN, Util.MONTH_DATE_PATTERN);
    }
    private void setCalendarMonth(String text){
        bar = (CustomFontTextView)findView(R.id.tv_calender_bar);
        bar.setText(text);
    }
    public void setData() {

        Log.d(TAG, "in setdata");
        calendarView.setMonthViewHorizontalSpacing(10);
        calendarView.setMonthViewVerticalSpacing(10);

        calendarView.setOnMonthChangeListener(new FlexibleCalendarView.OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month, @FlexibleCalendarView.Direction int direction) {
                //Toast.makeText(CalendarActivity3.this,""+year+" "+ (month+1),Toast.LENGTH_SHORT).show();
                String month_date_pattern = Util.convertDateFormat(getDate(year, month, 1), Util.DISCVER_SERVER_DATE_PATTERN, Util.MONTH_DATE_PATTERN);
                setCalendarMonth(month_date_pattern);

            }
        });

        calendarView.setCalendarView(new FlexibleCalendarView.CalendarView() {

            @Override
            public BaseCellView getCellView(int position, View convertView, ViewGroup parent, int cellType) {
                Log.d(TAG, "in base get cell view");
                BaseCellView cellView = (BaseCellView) convertView;
                if (cellView == null) {
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    cellView = (BaseCellView) inflater.inflate(R.layout.calender_date_cell_view, null);

                }
                return cellView;
            }

            @Override
            public BaseCellView getWeekdayCellView(int position, View convertView, ViewGroup parent) {
                Log.d(TAG, "in base get weekday");
                BaseCellView cellView = (BaseCellView) convertView;
                if (cellView == null) {
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    cellView = (BaseCellView) inflater.inflate(R.layout.calender_week_cell_view, null);

                }
                return cellView;
            }

            @Override
            public String getDayOfWeekDisplayValue(int dayOfWeek, String defaultValue) {
                return null;
            }
        });

        calendarView.setEventDataProvider(new FlexibleCalendarView.EventDataProvider() {
            @Override
            public List<Table1> getEventsForTheDay(int year, int month, int day) {
                Log.d(TAG, "in get Eventfortheday" + "");
                return getEvents(getDate(year, month, day));


            }
        });
        calendarView.setOnDateClickListener(new FlexibleCalendarView.OnDateClickListener() {
            @Override
            public void onDateClick(int year, int month, int day) {


                List<Table1> events = getEvents(getDate(year, month, day));

                if(events != null) {
                    EventsListFragment eventsListFragment = EventsListFragment.getInstance(events);
                    listener.startNext(eventsListFragment);
                }
                else{
                    Toast.makeText(getActivity(),"NO Events",Toast.LENGTH_SHORT).show();
                }
            }
        });
        calendarView.refresh();

    }

    private List<Table1> getEvents(String date) {
        return map.get(date);
    }


    private static String getDate(int year, int month, int day) {

        return year + "-" + String.format("%02d", (month + 1)) + "-" + String.format("%02d", day) + "T00:00:00";

    }

}
