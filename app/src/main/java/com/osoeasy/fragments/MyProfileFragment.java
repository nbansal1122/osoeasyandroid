package com.osoeasy.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.avenues.lib.testotpappnew.WebViewActivity;
import com.osoeasy.R;
import com.osoeasy.activities.BillingInfoActivity;
import com.osoeasy.activities.PaymentConfirmation;
import com.osoeasy.models.BaseApi;
import com.osoeasy.models.Photo.PhotoList;
import com.osoeasy.models.UserProfile.UserData;
import com.osoeasy.models.UserProfile.UserProfile;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import Utility.AvenuesParams;
import Utility.ServiceUtility;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;

/**
 * Created by Admin on 05-Aug-16.
 */
public class MyProfileFragment extends BaseFragment {
    private final static String ACCESS_CODE = "AVCS64DD72AH63SCHA";
    private final static String MERCHANT_ID = "94038";
    private final static String RSA_KEY_URL = "http://osoapi.azurewebsites.net/api/CCAvenuerRSA";
//    private final static String REDIRECT_URL = "http://122.182.6.216/merchant/ccavResponseHandler.jsp";
//    private final static String CANCEL_URL = "http://122.182.6.216/merchant/ccavResponseHandler.jsp";

    public final static String REDIRECT_URL = "http://osoapi.azurewebsites.net/api/CCAvenuerRSASuccess";
    public final static String CANCEL_URL = "http://osoapi.azurewebsites.net/api/CCAvenuerRSACancel";
    private static final int REQ_CCAVENUE = 1;
    private String orderId;

    private String payAmount;
    private ImageView imageView;
    private UserData userData;

    @Override
    public void initViews() {
        imageView = (ImageView) findView(R.id.iv_profile);
        getUserProfilePic();
        getData();
    }

    private void getData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.USER_PROFILE);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(UserProfile.class);
        executeTask(AppConstants.TASK_CODES.USER_PROFILE, object);
    }

    private void getUserProfilePic() {
        HttpParamObject obj = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.PHOTO_PROFILE, PhotoList.class);
        executeTask(AppConstants.TASK_CODES.PHOTO_PROFILE, obj);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_my_profile;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODES.USER_PROFILE) {
            UserProfile userProfile = (UserProfile) response;
            if (userProfile.getData().getUserData().size() > 0) {
                final UserData userData = userProfile.getData().getUserData().get(0);
                if (userData != null)
                    setData(userData);
            }
        } else if (response != null && taskCode == AppConstants.TASK_CODES.CONFIRM_PAYMENT) {
            BaseApi res = (BaseApi) response;
            if (!res.isError()) {
                getData();
            }
        }
        if (response != null && taskCode == AppConstants.TASK_CODES.PHOTO_PROFILE) {
            PhotoList list = (PhotoList) response;
            if (list.getData() != null && list.getData().getTable1() != null && list.getData().getTable1().size() > 0) {
                String imageUrl = list.getData().getTable1().get(0).getPhotoUrl();
                Picasso.with(getActivity()).load(imageUrl).into(imageView);
            }
        }
    }

    private void setData(UserData userData) {
        this.userData = userData;
        setText(R.id.tv_name, userData.getMembersName());
        setText(R.id.tv_email, Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        setText(R.id.tv_member_id, userData.getMemberCode());
//        setText(R.id.tv_username, userData.getUserName());
        setText(R.id.tv_society_id, userData.getSocietyId());
        setText(R.id.tv_father_name, userData.getFatherName());
        setText(R.id.tv_id_proof, userData.getIdProof());
        setText(R.id.tv_id_proof_number, userData.getIdProofNo());
        setText(R.id.tv_pay_mode, userData.getPayMode());
//        setText(R.id.tv_pay_ammount, String.valueOf(userData.getPayAmount()));
        setText(R.id.tv_pay_ammount, String.valueOf(userData.getPayAmount()));
        setText(R.id.tv_renewal_date, userData.getRenewalDate());
        if (userData.getPayAmount() > 0) {
            payAmount = userData.getPayAmount() + "";
            setOnClickListener(R.id.btn_payment);
        } else {
            hideVisibility(R.id.btn_payment);
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_payment:
                makePayment();
                break;
        }
    }

    private void makePayment() {
        orderId = ServiceUtility.randInt(0, 9999999) + "";
        Intent intent = new Intent(getActivity(), BillingInfoActivity.class);
        intent.putExtra(AvenuesParams.ACCESS_CODE, ACCESS_CODE);
        intent.putExtra(AvenuesParams.MERCHANT_ID, MERCHANT_ID);
        intent.putExtra(AvenuesParams.ORDER_ID, orderId);
        intent.putExtra(AvenuesParams.CURRENCY, "INR");
        intent.putExtra(AvenuesParams.AMOUNT, String.valueOf(userData.getPayAmount()));
        intent.putExtra(AvenuesParams.REDIRECT_URL, REDIRECT_URL);
        intent.putExtra(AvenuesParams.CANCEL_URL, CANCEL_URL);
        intent.putExtra(AvenuesParams.RSA_KEY_URL, RSA_KEY_URL);

        Bundle b = new Bundle();
        b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_SERIALIZABLE_OBJECT, userData);

        intent.putExtras(b);

        startActivityForResult(intent, REQ_CCAVENUE);
    }


    @Override
    protected void setText(int textViewID, String text) {
        if (TextUtils.isEmpty(text)) {
            text = "N/A";
        }
        super.setText(textViewID, text);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            startPayment(false, data);
            return;
        } else if (resultCode == Activity.RESULT_OK) {
            sendStatusToServer("success");
            startPayment(true, data);
            getData();
        } else if (resultCode == Activity.RESULT_FIRST_USER) {
            // Declined
            sendStatusToServer("failure");
            startPayment(false, data);
        }
    }

    private void startPayment(boolean isSuccess, Intent data) {
        Intent i = new Intent(getActivity(), PaymentConfirmation.class);
        Bundle b = data.getExtras();
        LinkedHashMap<String, String> map = new LinkedHashMap<>();

        map.put("Billing Name :", b.getString(AvenuesParams.BILLING_NAME));
        map.put("Mobile Number :", b.getString(AvenuesParams.BILLING_PHONE));
        map.put("Email :", b.getString(AvenuesParams.BILLING_EMAIL));
        map.put("Details Of Apartment :", b.getString(AvenuesParams.BILLING_ADDRESS));
        map.put("Description :", "Osoeasy ");
        map.put("Amount :", b.getString(AvenuesParams.AMOUNT));
        map.put("Transaction ID :", b.getString(AvenuesParams.ORDER_ID));
        map.put("Merchant Reference No :", b.getString(AvenuesParams.MERCHANT_ID));
        b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, getString(R.string.title_sucess_payment_subscription));
        if (isSuccess) {
            b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, getString(R.string.title_sucess_payment_subscription));
            map.put("Response Message :", "Transaction Successful");
        } else {
            b.putString(AppConstants.BUNDLE_KEYS.KEY_TITLE, getString(R.string.title_error_payment_subscription));
            map.put("Response Message :", "Transaction Failed");
        }

        b.putBoolean(AppConstants.BUNDLE_KEYS.IS_SUCCESS, isSuccess);
        b.putBoolean(AppConstants.BUNDLE_KEYS.BILLING_TYPE_SOCIETY, false);
        b.putSerializable(AppConstants.BUNDLE_KEYS.KEY_LINKED_MAP, map);
        i.putExtras(b);
        startActivity(i);
    }

    private void sendStatusToServer(String status) {
        HttpParamObject object = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.CONFIRM_PAYMENT, BaseApi.class);
        object.addParameter("status", status);
        object.addParameter("amount", payAmount);
        executeTask(AppConstants.TASK_CODES.CONFIRM_PAYMENT, object);
    }
}
