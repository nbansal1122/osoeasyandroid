package com.osoeasy.fragments;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.osoeasy.R;
import com.osoeasy.models.Video.VideoList;
import com.osoeasy.models.Video.table1;
import com.squareup.picasso.Picasso;

import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Sahil on 7/18/16.
 */
public class VideoFragment extends BaseFragment implements CustomListAdapterInterface, AdapterView.OnItemClickListener {

    private List<table1> table;
    private CustomListAdapter adapter;
    private GridView gridView;

    @Override
    public void initViews() {

        getData();

    }

    public void getData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.VIDEO_LIST);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(VideoList.class);
        executeTask(AppConstants.TASK_CODES.VIDEO, object);

    }

    public void setData() {
        gridView = (GridView) findView(R.id.gv_video);
        adapter = new CustomListAdapter(getActivity(), R.layout.row_video, table, this);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);

    }

    @Override
    public int getViewID() {
        return R.layout.fragment_video;
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        if (response != null && ((VideoList) response).getData().getTable1() != null) {
            table = ((VideoList) response).getData().getTable1();
            if (table != null && table.size() > 0) {
                for (table1 t : table) {
                    t.setImageUrl(getVideoImageFromVideoLink(t.getVd_vdlink()));
                }
            }
            setData();
        }
    }

    private String getVideoImageFromVideoLink(String videoUrl) {
        String defaultImageUrl = "http://img.youtube.com/vi/%s/default.jpg";
        if (!TextUtils.isEmpty(videoUrl)) {
            String[] array = videoUrl.split("v=");
            if (array != null && array.length > 1) {
                String url = array[1];
                String imageUrl = String.format(defaultImageUrl, url);
                return imageUrl;
            }
        }
        return "";
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.videoTitle.setText(table.get(position).getVd_vdtitle());
        String imgUrl = table.get(position).getImageUrl();
        Picasso.with(getActivity()).load(imgUrl).into(holder.videImage);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(table.get(position).getVd_vdlink()));
        startActivity(intent);
    }


    class Holder {
        CustomFontTextView videoLink, videoTitle;
        ImageView videImage;

        public Holder(View view) {

            videoTitle = (CustomFontTextView) view.findViewById(R.id.tv_video_title);
            videImage = (ImageView) view.findViewById(R.id.iv_video_link);
        }
    }

}
