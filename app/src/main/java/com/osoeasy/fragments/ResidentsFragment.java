package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.Resident.ResidentData;
import com.osoeasy.models.Resident.Table1;

import java.util.List;

import simplifii.framework.ListAdapters.ExpandableListAdapter;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by Sahil on 7/4/16.
 */
public class ResidentsFragment extends BaseFragment implements ExpandableListAdapter.ExpListInterface {

    ResidentData response;
    ExpandableListAdapter<Table1> adapter;
    private ExpandableListView listView;

    @Override
    public void initViews() {
        HttpParamObject object = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.RESIDENT_URL, ResidentData.class);
        object.setClassType(ResidentData.class);
        executeTask(AppConstants.TASK_CODES.RESIDENT, object);
    }

    private void setAdapter(List<Table1> table1List) {
        listView = (ExpandableListView) findView(R.id.lv_residents);
        adapter = new ExpandableListAdapter<>(getActivity(), table1List, this);
        listView.setAdapter(adapter);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            this.response = (ResidentData) response;
            List<Table1> table1List = ((ResidentData) response).getData().getTable1();
            if(table1List!=null){
                setAdapter(table1List);
            }
        }


    }

    @Override
    public int getViewID() {
        return R.layout.fragment_residents;
    }


    @Override
    public View getView(View convertView, Object child, LayoutInflater inflater) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_row_resident, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Table1 data = (Table1) child;
        holder.username.setText(data.getResidence());
        holder.email.setText(data.getEmailId() + "");
        holder.mobile.setText(data.getCellNo());
        holder.flat_details.setText(data.getType());
        return convertView;
    }

    private class ViewHolder {
        private TextView username, email, mobile, flat_details, flatDetailKeyText;

        ViewHolder(View v) {
            username = (TextView) v.findViewById(R.id.tv_member_name);
            email = (TextView) v.findViewById(R.id.tv_email);
            mobile = (TextView) v.findViewById(R.id.tv_mobile);
            flat_details = (TextView) v.findViewById(R.id.tv_flat_details);
            flatDetailKeyText = (TextView) v.findViewById(R.id.tv_key_flat_details);
            flatDetailKeyText.setText("Status");
        }
    }
}
