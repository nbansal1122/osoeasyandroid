package com.osoeasy.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.models.maintenance.Maintenance;
import com.osoeasy.models.maintenance.ServiceStatus;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;

/**
 * Created by mait on 7/4/16.
 */
public class MaintenanceFragment extends BaseFragment implements CustomListAdapterInterface {
    private List<Maintenance> maintenanceList = new ArrayList<>();
    private CustomListAdapter listAdapter;

    @Override
    public void initViews() {
        ListView listView = (ListView) findView(R.id.list_maintenance);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_maintenance, maintenanceList, this);
        listView.setAdapter(listAdapter);
        getdata();
    }

    private void getdata() {
        HttpParamObject httpParamObject = HttpParamObject.getDefaultObject(AppConstants.PAGE_URL.SERVICE_STATUS, ServiceStatus.class);
        executeTask(AppConstants.TASK_CODES.MAINTENANCE, httpParamObject);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null && taskCode == AppConstants.TASK_CODES.MAINTENANCE) {
            ServiceStatus serviceStatus = (ServiceStatus) response;

            final List<Maintenance> maintenances = serviceStatus.getData().getTable1();
            maintenanceList.clear();
            if (maintenances == null) return;
            for (Maintenance maintenance : maintenances) {
                maintenanceList.add(maintenance);
            }
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_maintenance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final Maintenance maintenance = maintenanceList.get(position);
        holder.tvAssetsName.setText(maintenance.getAssetName());
        holder.tvTitle.setText(maintenance.getServiceDescription());
        holder.tvScheduleDate.setText(maintenance.getScheduleDate());
        holder.tvStatus.setText(maintenance.getStatus());
        return convertView;
    }

    class Holder {
        TextView tvTitle, tvAssetsName, tvScheduleDate, tvStatus;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_maintenance_title);
            tvAssetsName = (TextView) view.findViewById(R.id.tv_assets_name);
            tvScheduleDate = (TextView) view.findViewById(R.id.tv_schedule_date);
            tvStatus = (TextView) view.findViewById(R.id.btn_status);
        }
    }
}
