package com.osoeasy.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.osoeasy.R;
import com.osoeasy.activities.FeeDetailActivity;
import com.osoeasy.models.home.HomeResponce;
import com.osoeasy.models.home.Timeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.ListAdapters.CustomListAdapter;
import simplifii.framework.ListAdapters.CustomListAdapterInterface;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.AppConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * Created by Admin on 14-Jun-16.
 */
public class HomeFragment extends BaseFragment implements CustomListAdapterInterface {
    ListView listView;
    List<Timeline> timelineList = new ArrayList<>();
    private CustomListAdapter listAdapter;
    private HomeResponce response;
    private NextFragmentListener listener;
    CustomFontTextView TotAnnouncement, TotComplainPending, TotMembers, TotPendingAmount, TotPendingApproval, TotPendingCount, TotPoll;

    public NextFragmentListener getListener() {
        return listener;
    }

    public void setListener(NextFragmentListener listener) {
        this.listener = listener;
    }

    public static HomeFragment getInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    private void getDashBoardData() {
        HttpParamObject object = new HttpParamObject();
        object.setUrl(AppConstants.PAGE_URL.DASH_BOARD_URL);
        object.addParameter("email", Preferences.getData(AppConstants.PREF_KEYS.EMAIL, ""));
        object.setClassType(HomeResponce.class);
        executeTask(AppConstants.TASK_CODES.HOME, object);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response != null) {
            this.response = (HomeResponce) response;
            if (this.response.getData() != null && this.response.getData().getTimeline() != null) {
                this.timelineList = ((HomeResponce) response).getData().getTimeline();
                setData();
            }
        }
    }

    @Override
    public void initViews() {
        getDashBoardData();
    }

    private void setData() {
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.view_header, null, false);
        listView = (ListView) findView(R.id.list_home);
        listView.addHeaderView(headerView);
        listAdapter = new CustomListAdapter(getActivity(), R.layout.row_home_list, timelineList, this);
        listView.setAdapter(listAdapter);
        initHeaders(headerView);
    }

    HashMap hashmap;

    private void initHeaders(View headerView) {
        hashmap = new HashMap<String, Integer>();

        if (response.getData().getTotAnnouncement() != null)
            hashmap.put("TotAnnouncement", response.getData().getTotAnnouncement());
        if (response.getData().getTotComplainPending() != null)
            hashmap.put("TotComplainPending", response.getData().getTotComplainPending());
        if (response.getData().getTotMember() != null)
            hashmap.put("TotMembers", response.getData().getTotMember());
        if (response.getData().getTotPendingAmount() != null)
            hashmap.put("TotPendingAmount", response.getData().getTotPendingAmount());
        if (response.getData().getTotPendingApproval() != null)
            hashmap.put("TotPendingApproval", response.getData().getTotPendingApproval());
        if (response.getData().getTotPendingCount() != null)
            hashmap.put("TotPendingCount", response.getData().getTotPendingCount());
        if (response.getData().getTotPoll() != null)
            hashmap.put("TotPoll", response.getData().getTotPoll());


        TotAnnouncement = (CustomFontTextView) findView(R.id.tv_announcements);
        TotComplainPending = (CustomFontTextView) findView(R.id.tv_pendingComplaints);
        TotMembers = (CustomFontTextView) findView(R.id.tv_residents);
        TotPendingAmount = (CustomFontTextView) findView(R.id.tv_pendingAmount);
//        TotPendingApproval = (CustomFontTextView)findView(R.id.tv_pendingApprovals);
//        TotPendingCount = (CustomFontTextView)findView(R.id.tv_pendingCount);
        TotPoll = (CustomFontTextView) findView(R.id.tv_easypoll);

        setTextFromMap(TotAnnouncement, "TotAnnouncement");
        setTextFromMap(TotComplainPending, "TotComplainPending");
        setTextFromMap(TotMembers, "TotMembers");
        setTextFromMap(TotPendingAmount, "TotPendingAmount");
        setTextFromMap(TotPoll, "TotPoll");
        setOnClickListener(R.id.ll_annonucements, R.id.ll_pending_amount, R.id.ll_easypoll, R.id.ll_pending_complaints, R.id.ll_pending_fee, R.id.ll_residents_list);
    }

    private void setTextFromMap(TextView tv, String key) {
        int count = hashmap.get(key) == null ? 0 : (int) hashmap.get(key);
        tv.setText("" + count);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        Fragment f = null;
        switch (v.getId()) {
            case R.id.ll_annonucements:
                f = FragmentFactory.getFragment(AppConstants.FRAGMENT_TYPE.NOTICE_BOARD);
                break;
            case R.id.ll_pending_amount:
                f = FragmentFactory.getFragment(AppConstants.FRAGMENT_TYPE.SOCIETY_BILL_PAYMENT);
                break;
            case R.id.ll_easypoll:
                f = FragmentFactory.getFragment(AppConstants.FRAGMENT_TYPE.EASY_POLL);
                break;
            case R.id.ll_pending_complaints:
                f = FragmentFactory.getFragment(AppConstants.FRAGMENT_TYPE.COMPLAINTS);
                break;
            case R.id.ll_pending_fee:
//                f = FragmentFactory.getFragment(AppConstants.FRAGMENT_TYPE.MY_PROFILE);
                startActivity(new Intent(getActivity(), FeeDetailActivity.class));
                return;
            case R.id.ll_residents_list:
                f = FragmentFactory.getFragment(AppConstants.FRAGMENT_TYPE.RESIDENTS);
                break;
        }
        if (f != null) {
            this.listener.startNext(f);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_home;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, int resourceID, LayoutInflater inflater) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resourceID, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        final Timeline timeline = timelineList.get(position);
        holder.tvTitle.setText(timeline.getnTitle());
        holder.tvSubTitle.setText(Html.fromHtml(timeline.getnType() + " " + timeline.getnDate()));
        holder.tvDetail.setText(timeline.getnDetails());
        return convertView;
    }

    class Holder {
        TextView tvTitle, tvSubTitle, tvDetail;

        public Holder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_row_home_title);
            tvSubTitle = (TextView) view.findViewById(R.id.tv_row_home_subtitle);
            tvDetail = (TextView) view.findViewById(R.id.tv_row_home_detail);
        }
    }
}
